#!/bin/sh
echo ------- Checking GLFW Libraries --------

GLFW_LIB=""

if locate libglfw3 >/dev/null; then
	echo glfw3 found
	GLFW_LIB="glfw3"
else
	if ldconfig -p | grep glfw >/dev/null; then
		echo glfw found
		GLFW_LIB="glfw"
	else
		echo glfw NOT FOUND
	fi	
fi

export GLFW_LIB

echo -------        End Seach        --------  
