#!/bin/bash

echo ---------- Compiling Shaders ----------

glslc shader.vert -o vert.spv
glslc shader.frag -o frag.spv

echo ----------        Done       ----------