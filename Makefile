#-----------------------------------------------------------------------#
# Program Structure 
#
# EXECUTABLE_NAME
#	Name of the produced executable
# DIR_SOURCE
#	Directory containing source files
# DIR_INCLUDE
#	Directory containing header files
# DIR_EXTERNAL
#	Directory containing external libraries
#
#
# Source files are obtained by the Makefile using wildcards (glob)
# Header files are included in produced executable.
#
#
# Compile Options
#
# C_FLAGS
#	Compile commands
# EXT
#	Include directories for runtime execution. By default, EXTERNAL
#	and SOURCE are added.
# LIBS
#	Linked libraries for the program.
#
# 
# Build Settings: Debug Mode
#
# DBGDIR
#	Desired build directory. Default is "build/debug"
# DBGEXE
#	Desire executable name for debug. Default is DBGDIR + / + EXECUTABLE_NAME 
# DBGCFLAGS
#	Debug compile flags. Default is "-g -DDEBUG"
#
# 
# Build Settings: Release Mode
#
# RELDIR
#	Desired build directory. Default is "build/release"
# RELEXE
#	Desire executable name for debug. Default is RELDIR + / + EXECUTABLE_NAME 
# RELCFLAGS
#	Release compile flags. Default is "-DNDEBUG"
#
#-----------------------------------------------------------------------#
SETUP_SCRIPTS=scripts

DIRECTORY != ./$(SETUP_SCRIPTS)/dir.sh
HOST ?= $(DIRECTORY)

#
# Application name and directories
#
DIR_INCLUDE     = $(HOST)/inc
DIR_EXTERNAL    = ext
DIR_BUILD       = $(HOST)/build
DIR_SOURCE      = $(HOST)/src
DIR_SOURCE_GAME = $(DIR_SOURCE)/game
DIR_SOURCE_GRAP = $(DIR_SOURCE)/graphics
DIR_SOURCE_PLAT = $(DIR_SOURCE)/platform
EXECUTABLE_NAME = dungeon
EXECUTABLE      = $(DIR_BUILD)/$(EXECUTABLE_NAME)

#
# Application Build + Source
#
SOURCE  = $(DIR_SOURCE_PLAT)/app_source.cpp
HEADERS = $(wildcard $(DIR_INCLUDE)/*.h)
OBJ     = ${SOURCE:.cpp=.o}

#
# Vulkan Build
#
VULKAN_SOURCE  = $(DIR_SOURCE_GRAP)/vulkan.cpp
VULKAN_HEADERS = $(wildcard $(DIR_INCLUDE)/*.h)
VULKAN_OBJ     = ${VULKAN_SOURCE:.cpp=.o}
VULKAN_LIB     = $(DIR_BUILD)/libvulkan.so

#
# Game Build
#
GAME_SOURCE  = $(DIR_SOURCE_GAME)/game_source.cpp
GAME_HEADERS = $(wildcard $(DIR_INCLUDE)/*.h)
GAME_OBJ     = ${GAME_SOURCE:.cpp=.o}
GAME_LIB     = $(DIR_BUILD)/libgame.so

#
# Vulkan Libs
#
VUL_INCLUDE = $(VULKAN_SDK_PATH)/include
VUL_LIB = -lvulkan

#
# Compiler Flags
#
EXT     = -I$(DIR_INCLUDE) -I$(DIR_EXTERNAL)
C_FLAGS = g++ -std=c++17

#
# Library Dependencies
#
RUNTIME  = -Wl,-rpath,$(HOST)/build/lib 
LINK     = -L$(HOST)/build/lib
STANDARD = -lX11 -lX11-xcb -ldl -lpthread -lXi -lm -l$(GLFW_LIB)
LIBS     = $(RUNTIME) $(LINK) $(STANDARD) $(VUL_LIB)

#
# Debug Build Settings
#
DBGEXE    = $(EXECUTABLE)
DBGOBJS   = $(OBJ)
DBGCFLAGS = -Wall -g -DDEBUG $(EXT)

#
# Release Build Settings
#
RELEXE    = $(EXECUTABLE)
RELOBJS   = $(OBJ)
#RELCFLAGS = -DNDEBUG $(EXT)
RELCFLAGS=-Wall -g -DDEBUG $(EXT)

#
# Resource Directories
#
SHADERDIR = resources/shaders

#-----------------------------------------------------------------------#
# Build commands:
#
# make
#	 Preps the build directory and then builds the program in debug mode
# make prep
#	 Creates the build directories for debug and release mode
# make debug
#     Builds the program with debug tools
# make release
#     Builds the program for release
# make run
#     Runs the program without debug info
# make test
#	 Runs the program with debug info
# make val
#     Runs the program with valgrind output.
# make clean
#     Removes all build files but not the build directories
# make remake
#	 Cleans build directory and remakes the program in debug mode
# make shaders
#     Makes the shader files listed in scripts/compiler_shaders.sh
# make gm
#     Compiles game code as *.so
# make clean_gm
#     Cleans game code and *.so
# make vk
#     Compiles vulkan code as *.so
# make clean_vk
#     Cleans vulkan code and *.so
# make pf
#     Makes the platform code and links the libs
# make clean_pf
#     Cleans platform code
#-----------------------------------------------------------------------#

.PHONY: all clean  \
	debug release  \
 	prep remake   \
	test val run   \
	gm vl pf       \
    clean_vk clean_gm clean_pf

all: prep release

#
# Debug Rules
#
debug: $(DBGEXE) vk gm

$(DBGEXE) : $(DBGOBJS) $(RELGAMELIB)
	$(C_FLAGS) $(DBGCFLAGS) $(EXT) $^ -o $@ $(LIBS)

$(DBGDIR)/%.o : %.cpp $(HEADERS)#
	$(C_FLAGS) $(DBGCFLAGS) $(EXT) -c $< -o $@

#
# Release Rules
#
release: $(RELEXE) vk gm

$(RELEXE) : $(RELOBJS) $(VULKAN_LIB) $(GAME_LIB) 
	$(C_FLAGS) $(EXT) $(RELCFLAGS) $^ -o $@ $(LIBS)

$(RELOBJS) : $(SOURCE) $(HEADERS)
	$(C_FLAGS) $(RELCFLAGS) $(EXT) -c $< -o $@

#
# Execution rules
#
run: $(RELEXE) $(VULKAN_LIB) $(GAME_LIB)
	$<

test: $(DBGEXE) $(VULKAN_LIB) $(GAME_LIB)
	$<

val: $(DBGEXE) $(VULKAN_LIB) $(GAME_LIB)
	cd $(DBGDIR); valgrind --leak-check=full $<

#
# Other rules
#
prep:
	@rm -rf $(DIR_BUILD);
	@mkdir -p $(DIR_BUILD)/src/platform $(DIR_BUILD)/src/game $(DIR_BUILD)/src/graphics;

remake: clean shaders prep all

shaders:
	@cd $(SHADERDIR); $(HOST)/scripts/compile_shaders.sh;

clean: clean_gm clean_vk
	@rm -rf build/
	@rm -f $(DIR_SOURCE_GRAP)/*.o
	@rm -f $(DIR_SOURCE_PLAT)/*.o
	@rm -f $(DIR_SOURCE_GAME)/*.o	

#
# Build Platform Code
#
# TODO(Dustin): yea...

clean_pf:
	@rm -f $(DIR_SOURCE_PLAT)/*.o
	@rm -f $(RELEXE)

#
# Build Vulkan Library
#
vk: $(VULKAN_LIB) 

$(VULKAN_LIB): $(VULKAN_OBJ)
	$(C_FLAGS) -fPIC $(RELCFLAGS) -shared $< -o $@

$(VULKAN_OBJ) : $(VULKAN_SOURCE) $(VULKAN_HEADERS)
	$(C_FLAGS) -fPIC $(RELCFLAGS) -c $< -o $@

clean_vk:
	@rm -f $(DIR_SOURCE_GRAP)/*.o
	@rm -f $(VULKAN_LIB)	

#
# Game Library
#
gm: $(GAME_LIB)

$(GAME_LIB): $(GAME_OBJ)
	$(C_FLAGS) -fPIC $(RELCFLAGS) -shared $< -o $@

$(GAME_OBJ) : $(GAME_SOURCE) $(GAME_HEADERS)
	$(C_FLAGS) -fPIC  $(RELCFLAGS) -c $< -o $@

clean_gm:
	@rm -f $(DIR_SOURCE_GAME)/*.o
	@rm -f $(GAME_LIB)