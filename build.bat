@echo off
setlocal EnableDelayedExpansion

:: External library directories
SET VULKAN_LIB_DIR="C:\VulkanSDK\1.1.121.2\Lib"
SET VULKAN_INC_DIR="C:\VulkanSDK\1.1.121.2\Include"
SET GLFW_LIB_DIR="C:\Program Files (x86)\Microsoft Visual Studio\2019\Libraries\glfw-3.3.bin.WIN64\lib-vc2019"
SET GLFW_INC_DIR="C:\Program Files (x86)\Microsoft Visual Studio\2019\Libraries\glfw-3.3.bin.WIN64\include"

:: Project directories
SET HOST_DIR=%~dp0
SET INC_DIR=%HOST_DIR%\inc
SET SRC_DIR=%HOST_DIR%\src
SET EXT_DIR=%HOST_DIR%\ext
SET BUILD_DIR=%HOST_DIR%\build

:: Project modules
SET GAME=game
SET GRAP=vulkan
SET PLAT=platform
SET TERRAIN=terrain_gen

:: Resources
SET TEXTURES=resources\textures
SET MODELS=resources\models
SET SHADERS=resources\shaders
SET TEXTURE_DIR=%HOST_DIR%\%TEXTURES%
SET MODEL_DIR=%HOST_DIR%\%MODELS%
SET SHADER_DIR=%HOST_DIR%\%SHADERS%

:: Lib, EXE, and source names
SET PLAT_SRC=%SRC_DIR%\%PLAT%\dungeon_win32.cpp
SET GRAP_SRC=%SRC_DIR%\%GRAP%\vulkan_win32.cpp
SET GAME_SRC=%SRC_DIR%\%GAME%\game_source.cpp
SET TERRAIN_SRC=%SRC_DIR%\%TERRAIN%\Terrain.cpp

SET PLAT_EXE=%BUILD_DIR%\dungeon
SET GRAP_LIB=%BUILD_DIR%\libvulkan.dll
SET GAME_LIB=%BUILD_DIR%\libgame.dll
SET TERRAIN_EXE=%BUILD_DIR%\terrain_gen

:: Linking and Compiling info
SET CFLAGS=-Zi /std:c++17 /EHsc /DDEBUG
SET LIB_PATH_DIR=/LIBPATH:%VULKAN_LIB_DIR% /LIBPATH:%GLFW_LIB_DIR%
SET GBL_LIB=libcpmt.lib user32.lib Gdi32.lib
SET PLAT_LIB=%LIB_PATH_DIR% glfw3dll.lib
SET VULKAN_LIB=%LIB_PATH_DIR% glfw3dll.lib vulkan-1.lib

SET INC=/I%INC_DIR% /I%EXT_DIR% /I%GLFW_INC_DIR% /I%VULKAN_INC_DIR%

:: Exports <- probably should remove this
SET VULKAN_EXPORTS=
for /f "delims=" %%x in (%SRC_DIR%/%GRAP%/vulkan_exports.txt) do set VULKAN_EXPORTS=-Export:%%x !VULKAN_EXPORTS!

:: Build commands
SET NONE_BUILD=""
SET CLEAN_BUILD=clean
SET GAME_BUILD=gm
SET VULK_BUILD=vk
SET PLAT_BUILD=plat
SET SHAD_BUILD=shaders
SET TEXT_BUILD=text
SET MODE_BUILD=mod
SET TERRAIN_BUILD=terrain

GOTO :MAIN
EXIT /B %ERRORLEVEL%

:: Helper Functions
::--------------------------------------------------
:: Creates the build directory for all files
:BUILD
    echo Creating build directory...
    1>NUL md %BUILD_DIR%

    1>NUL md %BUILD_DIR%\%PLAT%
    1>NUL md %BUILD_DIR%\%GAME%
    1>NUL md %BUILD_DIR%\%GRAP%

    1>NUL md %BUILD_DIR%\%TERRAIN%

    1>NUL md %BUILD_DIR%\resources
    1>NUL md %BUILD_DIR%\%TEXTURES%
    1>NUL md %BUILD_DIR%\%MODELS%
    1>NUL md %BUILD_DIR%\%SHADERS%
	1>NUL md %BUILD_DIR%\resources\terrain
       
	EXIT /B 0

:: Removes the build directory. Clears all build files
:Clean
    echo Cleaning build directory...
    1>NUL del /f /s /q %BUILD_DIR%
    1>NUL del /f %SHADER_DIR%\*.spv 
    EXIT /B 0

:: Builds the Game as a dll
:BuildGame
    echo Building game...
    pushd %BUILD_DIR%\%GAME%
        SET EXPORTS=-EXPORT:GameUpdate -EXPORT:GameInitialize -EXPORT:GameResize
		echo cl %CFLAGS% %INC% /I%SRC_DIR% %GAME_SRC% /LD /Fe%GAME_LIB% /link %VULKAN_LIB% %GBL_LIB% %EXPORTS%
        cl %CFLAGS% %INC% /I%SRC_DIR% %GAME_SRC% /LD /Fe%GAME_LIB% /link %VULKAN_LIB% %GBL_LIB% %EXPORTS%
    popd
    EXIT /B 0

:: Function to Build the Graphics as a dll
:BuildGraphics
    echo Building Vulkan...
    pushd %BUILD_DIR%\%GRAP%
		echo cl %CFLAGS% %INC% %GRAP_SRC% /LD /Fe%GRAP_LIB% /link %VULKAN_LIB% %GBL_LIB% %VULKAN_EXPORTS%
        cl %CFLAGS% %INC% %GRAP_SRC% /LD /Fe%GRAP_LIB% /link %VULKAN_LIB% %GBL_LIB% %VULKAN_EXPORTS%
    popd
    EXIT /B 0

:BuildPlatform
    echo Building platform layer...
    pushd %BUILD_DIR%\%PLAT%
        cl %CFLAGS% %INC% %PLAT_SRC% /Fe%PLAT_EXE% /link %PLAT_LIB% %GBL_LIB% 
    popd
    EXIT /B 0

:: Function to Build the shaders
:BuildShaders
    echo Building the shaders...
    pushd %SHADER_DIR%
        call %SHADER_DIR%\build.bat
    popd
    pushd %BUILD_DIR%\%SHADERS%
        xcopy /Y /D %SHADER_DIR%\*.spv .
    popd
    EXIT /B 0

:: Function to copy textures 
:CopyTextures
    echo Copying textures into build directory...
    pushd %BUILD_DIR%\%TEXTURES%
        echo %TEXTURE_DIR%
        1>NUL xcopy /D /e %TEXTURE_DIR%\* .  
    popd
    EXIT /B 0

:: Function to copy models
:CopyModels
    echo Copying models into build directory...
    pushd %BUILD_DIR%\%MODELS%
        1>NUL xcopy /D /e %MODEL_DIR%\* .  
    popd
    EXIT /B 0

:BuildTerrain
    echo Build terrain script...
	pushd %BUILD_DIR%\%TERRAIN%
		cl %CFLAGS% %INC% %TERRAIN_SRC% /Fe%TERRAIN_EXE%
    popd
    EXIT /B 0

:: Entry Point
::-----------------------------------------------
:MAIN
    IF "%1" == %NONE_BUILD% (
        echo Building everything...
        IF NOT EXIST %BUILD_DIR% (
            CALL :Build
        )

        CALL :Clean
        CALL :CopyTextures
        CALL :CopyModels
        CALL :BuildShaders
        CALL :BuildGame
        CALL :BuildPlatform
        EXIT /B %ERRORLEVEL%
    )

    IF NOT EXIST %BUILD_DIR% (
        CALL :BUILD
    )

    IF %1 == %GAME_BUILD% (
        CALL :BuildGame
        EXIT /B %ERRORLEVEL%
    )

    IF %1 == %PLAT_BUILD% (
        CALL :BuildPlatform
        EXIT /B %ERRORLEVEL%
    )

    IF %1 == %SHAD_BUILD% (
        CALL :BuildShaders
        EXIT /B %ERRORLEVEL%
    )

    IF %1 == %TEXT_BUILD% (
        CALL :CopyTextures
        EXIT /B %ERRORLEVEL%
    )

    IF %1 == %MODE_BUILD% (
        CALL :CopyModels
        EXIT /B %ERRORLEVEL%
    )
    
    IF %1 == %CLEAN_BUILD% (
        CALL :Clean
        EXIT /B %ERRORLEVEL%
    )

    IF %1 == %TERRAIN_BUILD% (
        CALL :BuildTerrain
        EXIT /B %ERRORLEVEL%
    )

    