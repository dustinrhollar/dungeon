#ifndef DUNGEON_PLATFORM
#define DUNGEON_PLATFORM

// void PlatformWaitForEvents();

#define PLATFORM_WAIT_FOR_EVENTS(name) void name()
typedef PLATFORM_WAIT_FOR_EVENTS(platform_wait_for_events);

typedef struct 
{
    int size;
    char *absoluteFilename;
    char *contents;
} FileInfo;

#define PLATFORM_READ_FILE(name) void name(FileInfo *info, const char *filename)
typedef PLATFORM_READ_FILE(platform_read_file);

#define PLATFORM_GET_REQUIRED_EXTENSIONS(name) std::vector<const char *> name(bool is_validation_enabled)
typedef PLATFORM_GET_REQUIRED_EXTENSIONS(platform_get_required_extensions);

#define PLATFORM_GET_FRAMEBUFFER_SIZE(name) void name(int *width, int *height)
typedef PLATFORM_GET_FRAMEBUFFER_SIZE(platform_get_frambuffer_size);

#define PLATFORM_CREATE_SURFACE(name) void name(VkSurfaceKHR *surface, VkInstance instance)
typedef PLATFORM_CREATE_SURFACE(platform_create_surface);

#define PLATFORM_GET_WALL_CLOCK(name) u64 name()
typedef PLATFORM_GET_WALL_CLOCK(platform_get_wall_clock);

#define PLATFORM_GET_SECONDS_ELAPSED(name) float name(u64 start, u64 end)
typedef PLATFORM_GET_SECONDS_ELAPSED(platform_get_seconds_elapsed);

// @param fullpath    pointer to a buffer the fullpath is copied into
// @param buffer_size size of the buffer
// @param filename    name of the file to get the full path for
// @param size        size of the filename
// @return the size of the string
#define PLATFORM_GET_FULL_FILEPATH(name) int name(char *fullpath,     \
size_t buffer_size, \
char *filename,     \
size_t size)
typedef PLATFORM_GET_FULL_FILEPATH(platform_get_full_filepath);


typedef struct 
{
    platform_wait_for_events         *WaitForEvents;
    platform_read_file               *ReadFile;
    platform_get_required_extensions *GetRequiredExtensions;
    platform_get_frambuffer_size     *GetFramebufferSize;
    platform_create_surface          *CreateSurface;
    platform_get_wall_clock          *GetWallClock;
    platform_get_seconds_elapsed     *GetSecondsElapsed;
    platform_get_full_filepath       *GetFullFilepath;
} platform_api;
extern platform_api Platform;

#endif