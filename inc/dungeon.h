
#ifndef DUNGEON_H
#define DUNGEON_H

#define GAME_INITIALIZE(name) void name(platform_api platform)
typedef GAME_INITIALIZE(game_initialize);
GAME_INITIALIZE(GameInitializeStub)
{
}

#define GAME_UPDATE(name) void name()
typedef GAME_UPDATE(game_update);
GAME_UPDATE(game_update_stub)
{
}


#define GAME_RESIZE(name) void name()
typedef GAME_RESIZE(game_resize);
GAME_RESIZE(GameResizeStub)
{
}


typedef struct
{
    game_update     *Update;
    game_initialize *Initialize;
    game_resize     *Resize;
    bool isValid;
} GameCode;
typedef GameCode GameApi;


#endif // DUNGEON_H