#ifndef APPCONFIG_H
#define APPCONFIG_H

#define global        static
#define local_persist static
#define internal      static

typedef int8_t  i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float  real32;
typedef double real64;

namespace Globals
{
    const char *app_name    = "Dungeon";
    const char *engine_name = "Maple";
    
    u8 engine_version[3] = {1, 0, 0};
    u8 app_version[3]    = {1, 0, 0};
}; // namespace globals

#endif // APPCONFIG_H
