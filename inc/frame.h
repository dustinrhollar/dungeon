#ifndef FRAME_INFO_H
#define FRAME_INFO_H

typedef enum
{
    DELETE_COMMAND_MESH,
    DELETE_COMMAND_TEXTURE,
} DeleteCommandType;

typedef struct 
{
    DeleteCommandType type;
    u32 id;
} DeleteCommand;

typedef struct 
{
    Mesh mesh;
    
    u32 textureCount;
    TextureInfo *textures;
} CreateCommand;

typedef struct 
{
    u64 meshId;
    // other important mesh info...
} DrawCommand;

typedef struct 
{
    std::vector<CreateCommand> newObjects;
    std::vector<DeleteCommand> deletedObjects;
    std::vector<DrawCommand> drawnObjects;
} FrameInfo;

#endif