/*
To include in your project, 
#define QUAD_TREE_IMPLEMENTATION
#include "quad_tree.h"

Data can be stored in the tree in two ways:
1. Data is only stored in the leaf nodes
2. Data is stored in all nodes

In both cases, a node will split when when the bin is
filled up.

By default data is stored in the leaf nodes, so to store
data in all locations, use the following define:
#define QUAD_TREE_DATA_ALL_NODES

*/


#ifdef QUAD_TREE_IMPLEMENTATION

#if !defined(QUAD_MALLOC) || !defined(QUAD_REALLOC) || !defined(QUAD_FREE)
#include <cstdlib>

#define QUAD_MALLOC  malloc
#define QUAD_REALLOC realloc
#define QUAD_FREE    free

#endif // def memory allocation macros

#define NODE_COUNT 4

// Each node recieves a bin of elements. BIN_SIZE defines the
// size of the bin
#ifndef BIN_SIZE
#define BIN_SIZE 10
#endif

// TODO(Dustin): Better handling of bounding region
struct AABB
{
    //-----------------------------------------
    // Data block
    //-----------------------------------------
    float min[3];
    float max[3];

    //-----------------------------------------
    // Function block
    //-----------------------------------------
    inline bool Intersection(AABB & boxA, AABB & boxB);
};
typedef AABB BoundingRegion;

bool BoundingRegion::Intersection(AABB & boxA, AABB & boxB)
{
    return true;
}

template <typename T>
struct Node
{
    //-----------------------------------------
    // Function blocks
    //-----------------------------------------
    Node() = default;
    Node(BoundingRegion region);
    Node(const Node<T> & copy);

    //-----------------------------------------
    // Data block
    //-----------------------------------------
    private:
    bool           is_leaf; // a leaf is at the bottom of a branch and has no children

    #if !defined(QUAD_TREE_DATA_ALL_NODES)
    // Reduce the total storage space by using a union of the children
    // and bins 
    union 
    {
        Node<T> children[NODE_COUNT]; // list of pointers to other heap allocated nodes
        T       bin[BIN_SIZE];    
    };
    #else
    // When data is not stored in the leafs, there has to be
    // two separate storage containers for the children and nodes;
    Node<T>        children[NODE_COUNT]; // list of pointers to other heap allocated nodes
    T              bin[BIN_SIZE];
    #endif // QUAD_TREE_DATA_ALL_NODES

    BoundingRegion bounds;

    //-----------------------------------------
    // Private function block
    //-----------------------------------------
    private:
#define SPLIT(name) inline void name(Node<T> & node)
    #if !defined(QUAD_TREE_DATA_ALL_NODES)
        SPLIT(SplitLeafData);
    #else
        SPLIT(SplitAllData);
    #endif // QUAD_TREE_DATA_ALL_NODES
#undef SPLIT
};

template <typename T>
struct QuadTree
{
    //-----------------------------------------
    // Function blocks
    //-----------------------------------------
    QuadTree() = default;
    QuadTree(float min_bound[3], float max_bound[3]);
    QuadTree(const QuadTree<T> & copy);

    //-----------------------------------------
    // Private Data block
    //-----------------------------------------
    private:
    Node<T> *root = nullptr; // heap allocated root of the tree
};

//------------------------------------------------------------------------//
// Node implmentation
//------------------------------------------------------------------------//
/*
I want the program to call this.split() on itself no matter the declaration
*/
#if !defined(QUAD_TREE_DATA_ALL_NODES)

template <typename T> 
void Node<T>::SplitLeafData(Node<T> & node)
{

}

#else

template <typename T> 
void Node<T>::SplitAllData(Node<T> & node)
{

}

#endif // QUAD_TREE_DATA_ALL_NODES

template <typename T>
Node<T>::Node(BoundingRegion region)
{
    this.bounds = region;

    // make sure the memory is freed up
    for (int i = 0; i < NODE_COUNT; ++i)
    {
        this.children[i] = nullptr;
    }
}

template <typename T>
Node<T>::Node(const Node<T> & copy)
{


    // make sure the memory is freed up
    for (int i = 0; i < NODE_COUNT; ++i)
    {
        this.children[i] = nullptr;
    }
}

//------------------------------------------------------------------------//
// QuadTree implmentation
//------------------------------------------------------------------------//
template <typename T>
QuadTree<T>::QuadTree(float min_bound[3], float max_bound[3])
{
    
}

template <typename T>
QuadTree<T>::QuadTree(const QuadTree<T> & copy)
{
    root = nullptr; // does this free the memory?
    root = copy.root;
}

#endif // QUAD_TREE_IMPLEMENTATION