#ifndef MAPLE_UTILS_ARRAY_LIST

#ifdef MAPLE_ARRAY_LIST_IMPLEMENTATION

#if !defined(ARRAY_MALLOC) || !defined(ARRAY_REALLOC) || !defined(ARRAY_FREE)
#include <cstdlib>

#define ARRAY_MALLOC  malloc
#define ARRAY_REALLOC realloc
#define ARRAY_FREE    free

#endif // def memory allocation macros

#if !defined(ARRAY_ASSERT)
#define ARRAY_ASSERT assert
#endif // ARRAY_ASSERT

#ifndef MAPLE_ARRAY_LIST_DEFAULT_SIZE

#define MAPLE_ARRAY_LIST_DEFAULT_SIZE 5

#endif // MAPLE_ARRAY_LIST_DEFAULT_SIZE

#define TEMPLATE template <typename T>

TEMPLATE
struct MArrayList
{
    //-----------------------------------------
    // Function block
    //-----------------------------------------
    public:
    MArrayList();
    MArrayList(unsigned int cap);
    MArrayList(T * start, T * end);
    MArrayList(const MArrayList<T> & copy);
    
    inline void Free();
    
    inline unsigned int Size()     {return this->size;}
    inline unsigned int Capacity() {return this.cap;}
    inline T *          Data()     {return list;}
    
    inline void SetCapacity(unsigned int cap) {this.cap = cap;}
    
    inline bool Add(unsigned int index, T element);
    inline bool Add(MArrayList<T> & other); // concatentate another arraylist to this one
    inline void Append(T element); // Adds an element to the end of the list
    
    inline T Remove(unsigned int index); // returns the removed element
    inline T Pop(); // removes an element from the end of the list
    
    inline T Set(unsigned int index, T element); // returnes the old element
    
    inline T Get(unsigned int index);
    
    // overloading operators
    inline T & operator[] (unsigned int);
    inline void  operator= (const MArrayList<T> & other);
    //inline operator T*() const {return list;}
    
    private:
    inline void Resize();
    
    //-----------------------------------------
    // Data  block
    //-----------------------------------------
    private:
    unsigned int size;
    unsigned int cap;
    T list[]; // heap allocated list
};


#endif // MAPLE_ARRAY_LIST_IMPLEMENTATION
#endif //  MAPLE_UTILS_ARRAY_LIST

// End of Header

TEMPLATE 
MArrayList<T>::MArrayList()
{
    this->size = 0;
    this->cap  = MAPLE_ARRAY_LIST_DEFAULT_SIZE;
    this->list = (T*)ARRAY_MALLOC(sizeof(T) * cap);
}

TEMPLATE 
MArrayList<T>::MArrayList(unsigned int cap)
{
    this->size = 0;
    this->cap  = cap;
    this->list = (T*)ARRAY_MALLOC(sizeof(T) * cap);
}

TEMPLATE 
MArrayList<T>::MArrayList(T * start, T * end)
{
    int size = end - start;
    if (size >= MAPLE_ARRAY_LIST_DEFAULT_SIZE)
        this->cap = size;
    else
        this->cap = MAPLE_ARRAY_LIST_DEFAULT_SIZE;
    
    this->size = size;
    this->list = (T*)ARRAY_MALLOC(sizeof(T) * cap);
    for (T * current = start; current != end; ++current)
    {
        this->list[i] = *current;
    }
}

//
TEMPLATE
MArrayList<T>::MArrayList(const MArrayList<T> & copy)
{
    this->list = nullptr;
    this->size = 0;
    this->cap  = 0;
    
    *this = copy;
}
//

TEMPLATE
void MArrayList<T>::Free()
{
    if (this->list)
        ARRAY_FREE(this->list);
    this->size = 0;
    this->cap  = 0;
    this->list = nullptr;
}


TEMPLATE
bool MArrayList<T>::Add(unsigned int index, T element)
{
    if (index >= this->size)
        return false;
    
    if (index >= this->cap)
        Resize();
    
    for (int i = index; i < this->size; ++i)
        list[i + 1] = list[i];
    
    list[index] = element;
    ++this->size;
}


TEMPLATE
void MArrayList<T>::Append(T element)
{
    if (this->size >= this->cap)
        Resize();
    
    list[this->size++] = element;
}

TEMPLATE
T MArrayList<T>::Remove(unsigned int index)
{
    ARRAY_ASSERT(index < this->size);
    
    T old = this->list[index];
    
    for (int i = size-1; i > index; --i)
        list[i - 1] = list[i];
    
    return old;
}

TEMPLATE
T MArrayList<T>::Pop()
{
    ARRAY_ASSERT(this->size != 0);
    
    return this->list[this->size--];
}

TEMPLATE
T MArrayList<T>::Set(unsigned int index, T element)
{
    ARRAY_ASSERT(index < this->size);
    
    T old = this->list[index];
    this->list[index] = element;
    return old;
}

TEMPLATE
T & MArrayList<T>::operator[] (unsigned int index)
{
    ARRAY_ASSERT(index < this->size);
    
    return list[index];
}

TEMPLATE
void MArrayList<T>::operator= (const MArrayList<T> & other)
{
    Free();
    
    this->size = other.Size();
    this->cap  = other.Capacity();
    
    ARRAY_MALLOC(sizeof(T) * other.Size());
    for (int i = 0; i < other.Size(); ++i)
    {
        this->list[i] = other[i];
    }
    
    return *this;
}


TEMPLATE
void MArrayList<T>::Resize()
{
    T *temp = ARRAY_MALLOC(sizeof(T) * this->cap * 2);
    for (int i = 0; i < this->size; ++i)
    {
        temp[i] = this->list[i];
    }
    
    ARRAY_FREE(this->list);
    this->list = temp;
}


#undef TEMPLATE
