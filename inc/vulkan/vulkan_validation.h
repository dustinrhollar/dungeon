#ifndef MAPLE_GRAPHICS_VULKAN_VALIDATION_LAYERS_H
#define MAPLE_GRAPHICS_VULKAN_VALIDATION_LAYERS_H

namespace vkMaple
{
    
    namespace Base
    {
        
        namespace Validation
        {
            
            global u32 validation_count = 1;
            global const char *validation_layers[] = {
                "VK_LAYER_KHRONOS_validation"};
            
#ifdef NDEBUG
            const bool enabled_validation_layers = false;
#else
            const bool enabled_validation_layers = true;
#endif
            
            bool CheckValidationLayerSupport();
            
        } // namespace Validation
        
    } // namespace Base
    
} // namespace vkMaple

#endif // MAPLE_GRAPHICS_VULKAN_VALIDATION_LAYERS_H