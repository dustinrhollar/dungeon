#ifndef MAPLE_GRAPHICS_VULKAN_DEVICE_H
#define MAPLE_GRAPHICS_VULKAN_DEVICE_H

namespace vkMaple 
{
    struct VulkanDevice 
    {
        private:
        struct QueueFamilies
        {
            VkQueue present_queue;
            VkQueue graphics_queue;
        };
        
        struct SyncObjects
        {
            VkSemaphore *image_available_semaphore = nullptr;
            VkSemaphore *render_finished_semaphore = nullptr;
            VkFence *in_flight_fences = nullptr;
        };
        
        struct QueueFamilyIndices
        {
            std::optional<u32> graphicsFamily;
            std::optional<u32> presentFamily;
            
            bool isComplete()
            {
                return graphicsFamily.has_value() && presentFamily.has_value();
            }
        };
        
        typedef struct
        {
            VkSurfaceCapabilitiesKHR capabilities;
            VkSurfaceFormatKHR *formats = nullptr;    // dynamic array
            VkPresentModeKHR *presentModes = nullptr; // dynamic array
        } SwapChainSupportDetails;
        
        
        //------------------------------------//
        // Function Block
        //------------------------------------//
        public:
        VulkanDevice() = default;
        void Initialize();
        void Shutdown();
        
        // Swapchain Creation
        
        SwapChainSupportDetails QuerySwapchainSupport(VkPhysicalDevice physical_device);
        
        void CreateSwapchain(VkSwapchainKHR &swapchain,
                             VkFormat       &image_format,
                             VkExtent2D     &extent,
                             VkImage        *images,
                             VkImageView    *image_views);
        void CreateFramebuffer(VkFramebufferCreateInfo framebufferInfo, VkFramebuffer *swapchain_framebuffers);
        
        void CreateImage(u32                   width, 
                         u32                   height, 
                         VkFormat              format, 
                         VkImageTiling         tiling, 
                         VkImageUsageFlags     usage, 
                         VkMemoryPropertyFlags properties, 
                         VkImage               &image, 
                         VkDeviceMemory        &imageMemory);
        
        VkImageView CreateImageView(VkImage            image, 
                                    VkFormat           format,
                                    VkImageAspectFlags aspectFlags);
        
        void TransitionImageLayout(VkImage       image, 
                                   VkFormat      format, 
                                   VkImageLayout oldLayout, 
                                   VkImageLayout newLayout);
        
        VkFormat FindDepthFormat();
        
        void CreateRenderPass(VkRenderPass *render_pass, 
                              VkFormat swapchain_image_format);
        
        void CreateCommandbuffer(VkCommandBuffer *buffers, u32 count);
        
        VkCommandBuffer BeginSingleTimeCommands();
        void EndSingleTimeCommands(VkCommandBuffer command_buffer);
        u32  FindMemoryType(u32 typeFilter, VkMemoryPropertyFlags properties);
        
        void CreateFramebuffers();
        void CreateCommandBuffers();
        
        // Render pass creation
        void CreateRenderPass(VkRenderPass & render_pass, VkFormat image_format); // image_format is the swapchain image_format
        
        // Buffer creation
        void CreateBuffer(VkDeviceSize          size, 
                          VkBufferUsageFlags    usage, 
                          VkMemoryPropertyFlags properties, 
                          VkBuffer              &buffer, 
                          VkDeviceMemory        &bufferMemory);
        void CreateBufferAsStaging(VkBufferUsageFlags usage,
                                   VkBuffer           *buffer, 
                                   VkDeviceMemory     *buffer_memory, 
                                   void *buffer_data, u32 size);
        
        void CopyBuffer(VkBuffer srcBuffer, 
                        VkBuffer dstBuffer, 
                        VkDeviceSize size);
        
        // Image creation
        
        
        // Descriptors
        void CreateDescriptorPool(VkDescriptorPool *descriptor_pool, 
                                  VkDescriptorPoolSize *pool_sizes,
                                  u32 pool_size_count,
                                  u32 swapchain_count);
        void CreateDescriptorSetLayout(VkDescriptorSetLayout *descriptorset_layout, 
                                       VkDescriptorSetLayoutBinding *bindings,
                                       u32 binding_count);
        void CreateDescriptorSets(VkDescriptorSet *sets,
                                  VkDescriptorSetAllocateInfo allocInfo);
        
        void UpdateDescriptorSets(VkWriteDescriptorSet *descriptor_set_writes,
                                  u32 write_count,
                                  u32 copy_count = 0,
                                  VkCopyDescriptorSet* pstop = nullptr);
        
        // Pipeline creation
        VkShaderModule CreateShaderModule(const char *code, int size);
        void DestroyShaderModule(VkShaderModule module);
        void CreatePipelineLayout(VkPipelineLayoutCreateInfo *create_info, VkPipelineLayout *layout);
        void CreateGraphicsPipeline(VkGraphicsPipelineCreateInfo *pipeline_info, 
                                    VkPipeline *pipeline);
        
        void CreateInstance();
        void SetupDebugMessenger();
        void PickPhysicalDevice();
        void CreateLogicalDevice();
        void CreateCommandPool();
        void CreateSyncObjects();
        
        bool IsDeviceSuitable(VkPhysicalDevice physical_device);
        QueueFamilyIndices FindQueueFamilies(VkPhysicalDevice physical_device);
        bool CheckDeviceExtensionSupport(VkPhysicalDevice physical_device);
        
        void PopulateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo);
        void DestroyDebugUtilsMessengerEXT(VkDebugUtilsMessengerEXT debugMessenger, 
                                           const VkAllocationCallbacks* pAllocator);
        VkResult CreateDebugUtilsMessengerEXT(const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, 
                                              const VkAllocationCallbacks* pAllocator, 
                                              VkDebugUtilsMessengerEXT* pDebugMessenger);
        
        // Shutdown routines
        void DestroyImageView(VkImageView view);
        void DestroyImage(VkImage image);
        void FreeMemory(VkDeviceMemory memory);
        void DestroyFramebuffer(VkFramebuffer buffer);
        void FreeCommandBuffers(VkCommandBuffer *buffer, u32 buffer_count);
        void DestroySwapchainKHR(VkSwapchainKHR swapchain);
        void DestroyRenderPass(VkRenderPass render_pass);
        
        // Map Memory
        void Map(void **mapped_memory, VkDeviceMemory memory);
        void Unmap(VkDeviceMemory memory);
        
        // Idling
        void Idle(); // blocks until the device finishes rendering last frame
        
        //------------------------------------//
        // Data Block
        //------------------------------------//
        VkDebugUtilsMessengerEXT debug_messenger;
        
        VkInstance               instance;
        VkSurfaceKHR             surface;
        QueueFamilies            queue_families;
        
        VkPhysicalDevice         physical_device;
        VkDevice                 device;
        VkPhysicalDeviceLimits   limits;
        
        // TODO(Dustin): Move out of here?
        VkCommandPool            command_pool;
        
        SyncObjects              sync_objects;
    };
} // vkMaple

#endif // MAPLE_GRAPHICS_VULKAN_DEVICE_H