#ifndef MAPLE_GRAPHICS_VULKAN_COMMAND_H
#define MAPLE_GRAPHICS_VULKAN_COMMAND_H

namespace vkMaple
{
    
    namespace Base
    {
        
        VkCommandBuffer BeginSingleTimeCommands(VkDevice device, VkCommandPool command_pool);
        void EndSingleTimeCommands(VkDevice device, VkQueue graphicsQueue, VkCommandBuffer commandBuffer,
                                   VkCommandPool command_pool);
        
        void CreateCommandBuffers(Swapchain *swapchain, VkDevice device, 
                                  VkCommandPool command_pool, u32 count);
        
    } // Base
    
} // vkMaple

#endif // MAPLE_GRAPHICS_VULKAN_COMMAND_H