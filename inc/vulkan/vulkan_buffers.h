#ifndef MAPLE_GRAPHICS_VULKAN_BUFFERS_H
#define MAPLE_GRAPHICS_VULKAN_BUFFERS_H

namespace vkMaple {
    
    namespace Base {
        
        void CreateBuffer(Device device_handle, VkDeviceSize size, VkBufferUsageFlags usage, 
                          VkMemoryPropertyFlags properties, VkBuffer& buffer, 
                          VkDeviceMemory& bufferMemory);
        
        void CreateUniformBuffer(Uniforms *uniforms, VkDeviceSize buffer_size, VkBufferUsageFlags usage, 
                                 VkMemoryPropertyFlags properties, Device device_handle, u32 swapchain_count);
        //void CreateDynamicUniformBuffers(DynamicUniforms *uniforms, Device device_handle, u32 count, u32 swapchain_count);
        
        void CreateVertexBuffer(Device device_handle, VkCommandPool command_pool, VkQueue graphics_queue,
                                VkBuffer *vertex_buffer, VkDeviceMemory *vertex_buffer_memory,
                                void *indices, u32 size);
        void CreateIndexBuffer(Device device_handle, VkCommandPool command_pool, VkQueue graphics_queue,
                               VkBuffer *index_buffer, VkDeviceMemory *index_buffer_memory,
                               void *indices, u32 size);
        
        void CopyBuffer(VkDevice device, VkCommandPool command_pool, VkQueue graphics_queue,
                        VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);
    } // Base
    
} // vkMaple

#endif // MAPLE_GRAPHICS_VULKAN_BUFFERS_H