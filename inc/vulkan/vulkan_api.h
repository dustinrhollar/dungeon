#ifndef MAPLE_GRAPHICS_VULKAN_H
#define MAPLE_GRAPHICS_VULKAN_H

namespace vkMaple
{
    
    void Resize();
    void Shutdown();
    
    // Descriptors
    void CreateDescriptorSetLayout(VkDescriptorSetLayoutBinding *bindings,
                                   u32 binding_count);
    void CreateDescriptorPool(VkDescriptorPoolSize *pool_sizes, u32 pool_size_count);
    void CreateDescriptorSets(u32 size);
    void BindDescriptors();
    
    void Map(void **mapped_memory);
    void Unmap();
    
    // Buffers
    void CreateUniformBuffer(VkDeviceSize buffer_size, VkBufferUsageFlags usage, 
                             VkMemoryPropertyFlags properties);
    
    // Per Frame 
    void BeginFrame(float color[3]);
    void EndFrame();
    
    typedef struct
    {
        u32 vert_size;
        u32 frag_size;
        char *vert_code;
        char *frag_code;
        
        // TODO(Dustin): Stride for vertex...
    } PipelineCreateInfo;
    int CreatePipeline(PipelineCreateInfo pipeline_create_info);
    
    typedef struct
    {
        bool is_indexed;
        u32 mesh_id;
    } DrawMeshInfo;
    void DrawMesh(DrawMeshInfo mesh_info);
    
    void BindPipeline(u32 pipeline_id);
    
} // vkMaple

#endif // MAPLE_GRAPHICS_VULKAN_H