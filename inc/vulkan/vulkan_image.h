#ifndef MAPLE_GRAPHICS_VULKAN_IMAGE_H
#define MAPLE_GRAPHICS_VULKAN_IMAGE_H

namespace vkMaple {
    
    namespace Base {
        u32 FindMemoryType(VkPhysicalDevice physicalDevice, u32 typeFilter, VkMemoryPropertyFlags properties);
        
        void CreateImage(Device device_handle, u32 width, u32 height, VkFormat format, 
                         VkImageTiling tiling, VkImageUsageFlags usage, 
                         VkMemoryPropertyFlags properties, VkImage &image, 
                         VkDeviceMemory &imageMemory);
        VkImageView CreateImageView(VkDevice device, VkImage image, VkFormat format,
                                    VkImageAspectFlags aspectFlags);
        
        void TransitionImageLayout(VkDevice device, VkQueue graphicsQueue, VkCommandPool command_pool,
                                   VkImage image, VkFormat format, 
                                   VkImageLayout oldLayout, VkImageLayout newLayout);
        
    } // Base
    
} // vkMaple

#endif // MAPLE_GRAPHICS_VULKAN_IMAGE_H