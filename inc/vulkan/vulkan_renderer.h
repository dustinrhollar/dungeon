#ifndef MAPLE_VULKAN_RENDERER_H
#define MAPLE_VULKAN_RENDERER_H

namespace vkMaple
{
    struct Renderer
    {
        //------------------------------------//
        // Function Block
        //------------------------------------//
        public:
        Renderer() = default;
        void SetBackgroundColor(float color[3]);
        
        void Initialize();
        void Shutdown();
        
        void BeginFrame();
        void EndFrame();
        void Resize();
        
        size_t GetMinMemoryAlignment() 
        {
            return device.limits.minUniformBufferOffsetAlignment;
        }
        
        
        //------------------------------------//
        // Data Block
        //------------------------------------//
        public:
        
        // Per frame information
        struct _Frame
        {
            int      current_frame = 0;
            u32      image_index;
            u32      current_ubo_offset = 0;
            VkResult khr_result;
            VulkanPipeline bound_pipeline;
        } frame;
        
        public:
        VulkanDevice    device;
        VulkanSwapchain swapchain;
        VkRenderPass    render_pass;
        
        // Allocator
        VmaAllocator allocator;
        
        float background_color[3];
    };
    
} // vkMaple

#endif // MAPLE_VULKAN_RENDERER_H
