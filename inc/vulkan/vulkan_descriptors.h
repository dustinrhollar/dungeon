#ifndef MAPLE_GRAPHICS_VULKAN_DESCRIPTORS_H
#define MAPLE_GRAPHICS_VULKAN_DESCRIPTORS_H

namespace vkMaple {
    
    // Descriptor Layout
    enum DescritporLayoutType
    {
        DESCRIPTOR_LAYOUT_TYPE_OPAQUE      = 0,
        DESCRIPTOR_LAYOUT_TYPE_TRANSLUCENT = 1,
    };
    
    struct Uniform
    {
        size_t min_ubo_alignment;
        
        VkDeviceSize          buffer_size; 
        VkBufferUsageFlags    usage; 
        VkMemoryPropertyFlags properties;
        
        // Buffers are persistently mapped
        VkBuffer          *buffers    = nullptr;
        VmaAllocation     *allocation = nullptr;
        VmaAllocationInfo *alloc_info = nullptr;
    };
    
    struct DescriptorPool
    {
        VkDescriptorPool     pool;
        VkDescriptorPoolSize *sizes = nullptr;
    };
    
    
    struct DescriptorSet
    {
        u32                   layout_buffer_size; // <- needed?
        VkDescriptorSetLayout layout;
        VkDescriptorSet       *sets = nullptr;
        
        void Bind(VkCommandBuffer & buffer,
                  VkPipelineLayout & pipeline_layout,
                  u32 image_index,
                  u32 first_set,
                  u32 descriptor_set_count,
                  u32 dynamic_offset_count = 0,
                  const u32 *p_dynamic_offsets = nullptr)
        {
            vkCmdBindDescriptorSets(buffer, 
                                    VK_PIPELINE_BIND_POINT_GRAPHICS, 
                                    pipeline_layout, 
                                    first_set, 
                                    descriptor_set_count, 
                                    &sets[image_index], 
                                    dynamic_offset_count,
                                    p_dynamic_offsets);
        }
    };
    
    
    /*
    void DescriptorSet::Bind(VkCommandBuffer & buffer,
                             u32 image_index,
                             u32 first_set,
                             u32 descriptor_set_count,
                             u32 dynamic_offset_count = 0,
                             const u32 *p_dynamic_offsets = nullptr)
    {
    
    }
    */
    
} // vkMaple



#endif // MAPLE_GRAPHICS_VULKAN_DESCRIPTORS_H