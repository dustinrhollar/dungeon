#ifndef MAPLE_GRAPHICS_VULKAN_SWAPCHAIN_H
#define MAPLE_GRAPHICS_VULKAN_SWAPCHAIN_H

namespace vkMaple
{
    typedef struct
    {
        VkImage image;
        VkDeviceMemory image_memory;
        VkImageView image_view;
        
        void Shutdown(VulkanDevice & device)
        {
            device.DestroyImageView(image_view);
            device.DestroyImage(image);
            device.FreeMemory(image_memory);
        }
    } DepthResources;
    
    struct VulkanSwapchain
    {
        
        //------------------------------------//
        // Function Block
        //------------------------------------//
        VulkanSwapchain() = default;
        
        void Initialize(VulkanDevice & device);
        void CreateFramebuffers(VulkanDevice & device, VkRenderPass & render_pass);
        void CreateCommandbuffers(VulkanDevice & device);
        void Recreate(VulkanDevice & device);
        void Cleanup(VulkanDevice & device);
        
        //------------------------------------//
        // Data Block
        //------------------------------------//
        VkSwapchainKHR swapchain;
        VkFormat image_format;
        VkExtent2D extent;
        
        // Depth information
        DepthResources depth_resources;
        
        // dynamic arrays
        VkImage         *images = nullptr;
        VkImageView     *image_views = nullptr;
        VkFramebuffer   *framebuffers = nullptr;
        VkCommandBuffer *command_buffers = nullptr;
    };
    
} // namespace vkMaple

#endif