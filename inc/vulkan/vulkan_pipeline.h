#ifndef MAPLE_GRAPHICS_VULKAN_PIPELINE_H
#define MAPLE_GRAPHICS_VULKAN_PIPELINE_H

namespace vkMaple {
    
    typedef struct
    {
        u32 vert_size;
        u32 frag_size;
        char *vert_code;
        char *frag_code;
        
        // Per vertex information
        u32 vertex_desc_count;
        u32 attrib_count;
        VkVertexInputBindingDescription   *vertex_desc;
        VkVertexInputAttributeDescription *attribute_descriptions;
    } PipelineCreateInfo;
    
    
    VkVertexInputAttributeDescription CreateVertexAttributeDescription(u32 binding, 
                                                                       u32 location, 
                                                                       VkFormat format,
                                                                       u32 offset)
    {
        VkVertexInputAttributeDescription attrib = {};
        attrib.binding = binding;
        attrib.location = location;
        attrib.format = format;
        attrib.offset = offset;
        
        return attrib;
    }
    
    VkVertexInputBindingDescription CreateVertexBindingDescription(u32 binding, 
                                                                   u32 stride, 
                                                                   VkVertexInputRate input_rate)
    {
        VkVertexInputBindingDescription bindingDescription = {};
        bindingDescription.binding = binding;
        bindingDescription.stride = stride;
        bindingDescription.inputRate = input_rate;
        
        return bindingDescription;
    }
    
    struct VulkanPipeline
    {
        //------------------------------------//
        // Function Block
        //------------------------------------//
        public:
        void Initialize(PipelineCreateInfo create_info, 
                        VkDescriptorSetLayout descriptor_set_layout,
                        VkRenderPass render_pass,
                        VkExtent2D swapchain_extent,
                        VulkanDevice & device)
        {
            VkShaderModule vertShaderModule = device.CreateShaderModule(create_info.vert_code, create_info.vert_size);
            VkShaderModule fragShaderModule = device.CreateShaderModule(create_info.frag_code, create_info.frag_size);
            
            // Shader pipeline
            VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
            vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
            vertShaderStageInfo.module = vertShaderModule;
            vertShaderStageInfo.pName = "main";
            
            VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
            fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
            fragShaderStageInfo.module = fragShaderModule;
            fragShaderStageInfo.pName = "main";
            
            VkPipelineShaderStageCreateInfo shaderStages[] = {
                vertShaderStageInfo,
                fragShaderStageInfo
            };
            
            // Input Assembly
            VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
            vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
            vertexInputInfo.vertexBindingDescriptionCount   = create_info.vertex_desc_count;
            vertexInputInfo.pVertexBindingDescriptions      = create_info.vertex_desc;
            vertexInputInfo.vertexAttributeDescriptionCount = create_info.attrib_count;
            vertexInputInfo.pVertexAttributeDescriptions    = create_info.attribute_descriptions;
            
            VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
            inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
            inputAssembly.topology               = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
            inputAssembly.primitiveRestartEnable = VK_FALSE;
            
            VkViewport viewport = {};
            viewport.x = 0.0f;
            viewport.y = 0.0f;
            viewport.width  = (float) swapchain_extent.width;
            viewport.height = (float) swapchain_extent.height;
            viewport.minDepth = 0.0f;
            viewport.maxDepth = 1.0f;
            
            VkRect2D scissor = {};
            scissor.offset = {0, 0};
            scissor.extent = swapchain_extent;
            
            VkPipelineViewportStateCreateInfo viewportState = {};
            viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
            viewportState.viewportCount = 1;
            viewportState.pViewports = &viewport;
            viewportState.scissorCount = 1;
            viewportState.pScissors = &scissor;
            
            // Rasterizer
            VkPipelineRasterizationStateCreateInfo rasterizer = {};
            rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
            rasterizer.depthClampEnable = VK_FALSE;
            rasterizer.rasterizerDiscardEnable = VK_FALSE;
            rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
            rasterizer.lineWidth = 1.0f;
            //rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
            rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
            rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
            rasterizer.depthBiasEnable = VK_FALSE;
            rasterizer.depthBiasConstantFactor = 0.0f; // Optional
            rasterizer.depthBiasClamp = 0.0f; // Optional
            rasterizer.depthBiasSlopeFactor = 0.0f; // Optional
            
            // Multisampling
            VkPipelineMultisampleStateCreateInfo multisampling = {};
            multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
            multisampling.sampleShadingEnable = VK_FALSE;
            multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
            multisampling.minSampleShading = 1.0f; // Optional
            multisampling.pSampleMask = nullptr; // Optional
            multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
            multisampling.alphaToOneEnable = VK_FALSE; // Optional
            
            
            // Depth/Stencil Testing - not right now
            VkPipelineDepthStencilStateCreateInfo depthStencil = {};
            depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
            depthStencil.depthTestEnable = VK_TRUE;
            depthStencil.depthWriteEnable = VK_TRUE;
            depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
            depthStencil.depthBoundsTestEnable = VK_FALSE;
            depthStencil.minDepthBounds = 0.0f; // Optional
            depthStencil.maxDepthBounds = 1.0f; // Optional
            depthStencil.stencilTestEnable = VK_FALSE;
            depthStencil.front = {}; // Optional
            depthStencil.back = {}; // Optional
            
            // Color Blending
            VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
            colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | 
                VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
            colorBlendAttachment.blendEnable = VK_FALSE;
            
            VkPipelineColorBlendStateCreateInfo colorBlending = {};
            colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
            colorBlending.logicOpEnable = VK_FALSE;
            colorBlending.logicOp = VK_LOGIC_OP_COPY;
            colorBlending.attachmentCount = 1;
            colorBlending.pAttachments = &colorBlendAttachment;
            colorBlending.blendConstants[0] = 0.0f;
            colorBlending.blendConstants[1] = 0.0f;
            colorBlending.blendConstants[2] = 0.0f;
            colorBlending.blendConstants[3] = 0.0f;
            
            VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
            pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
            pipelineLayoutInfo.setLayoutCount = 1; // Optional
            pipelineLayoutInfo.pSetLayouts = &descriptor_set_layout; // Optional
            pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
            pipelineLayoutInfo.pPushConstantRanges = nullptr; // Optional
            
            device.CreatePipelineLayout(&pipelineLayoutInfo, &layout);
            
            VkDynamicState dynamic_states[] = {
                VK_DYNAMIC_STATE_VIEWPORT,
                VK_DYNAMIC_STATE_SCISSOR
            };
            
            VkPipelineDynamicStateCreateInfo dynamic_state_info;
            dynamic_state_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
            dynamic_state_info.pNext = nullptr;
            dynamic_state_info.flags = 0;
            dynamic_state_info.dynamicStateCount = 2;
            dynamic_state_info.pDynamicStates = dynamic_states;
            
            VkGraphicsPipelineCreateInfo pipelineInfo = {};
            pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
            pipelineInfo.stageCount = 2;
            pipelineInfo.pStages = shaderStages;
            pipelineInfo.pVertexInputState = &vertexInputInfo;
            pipelineInfo.pInputAssemblyState = &inputAssembly;
            pipelineInfo.pViewportState = &viewportState;
            pipelineInfo.pRasterizationState = &rasterizer;
            pipelineInfo.pMultisampleState = &multisampling;
            pipelineInfo.pDepthStencilState = &depthStencil; // Optional
            pipelineInfo.pColorBlendState = &colorBlending;
            pipelineInfo.pDynamicState = &dynamic_state_info; // Optional
            pipelineInfo.layout = layout;
            pipelineInfo.renderPass = render_pass;
            pipelineInfo.subpass = 0;
            pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
            pipelineInfo.basePipelineIndex = -1; // Optional
            
            device.CreateGraphicsPipeline(&pipelineInfo, &pipeline);
            
            device.DestroyShaderModule(vertShaderModule);
            device.DestroyShaderModule(fragShaderModule);
        }
        
        void Bind(VkCommandBuffer command_buffer)
        {
            vkCmdBindPipeline(command_buffer, 
                              VK_PIPELINE_BIND_POINT_GRAPHICS, 
                              pipeline);
        }
        
        //------------------------------------//
        // Function Block
        //------------------------------------//
        public:
        // Pipeline Descriptor Info
        u32 descriptor_layout_buffer_size; // TODO(Dustin): Needed?
        VkDescriptorSetLayout descriptor_layout; // TODO(Dustin): Needed?
        
        // NOTE(Dustin): Place descriptor set here?
        
        // Pipeline info
        VkPipelineLayout layout;
        VkPipeline pipeline;
    };
    
    
    namespace Base {
        
        // Hmmm....
        /*
        typedef struct 
        {
        
        } RenderPass;
        
        typedef struct
        {
            // Pipeline Descriptor Info
            u32 descriptor_layout_buffer_size;
            VkDescriptorSetLayout descriptor_layout;
            
            // Pipeline info
            VkPipelineLayout layout;
            VkPipeline pipeline;
            
            PipelineCreateInfo pipeline_info;
        } Pipeline;
        
        void CreateRenderPass(VkRenderPass *render_pass, Device device_handle, VkFormat swapchain_image_format);
        void CreateGraphicsPipeline(Pipeline *pipeline, VkDevice device, VkRenderPass render_pass, 
                                    VkDescriptorSetLayout descriptor_set_layout, 
                                    VkExtent2D swapchain_extent, PipelineCreateInfo pipeline_info);
        */
    }
    
}

#endif // MAPLE_GRAPHICS_VULKAN_PIPELINE_H
