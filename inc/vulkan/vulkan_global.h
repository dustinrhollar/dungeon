#ifndef MAPLE_GRAPHICS_VULKAN_GLOBAL_H
#define MAPLE_GRAPHICS_VULKAN_GLOBAL_H

namespace vkMaple
{
    global bool framebuffer_resized = false;
    
    global const int MAX_FRAMES_IN_FLIGHT = 2;
    
    global u32 device_extensions_count = 1;
    global const char *device_extensions[] = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };
    
    
    // key is the id of the mesh provided by a client 
    //
    
    typedef struct 
    {
        void *vertices;
        u32 vert_count;
        u32 vert_size;
        
        void *indices;
        u32 index_count;
        u32 index_size;
        
        u32 mesh_id;
    } CreateMeshInfo;
    
    //global std::unordered_map<u32, MeshBuffer> GlobalMeshList;
    //
    
    global std::unordered_map<u32, VulkanPipeline> GlobalPipelineList;
    global u32 GlobalPipelineCount = 0;
    
    
    // Global Vulkan State
    //extern VulkanApplication GlobalVulkanState;
    
} // namespace vkMaple

#endif // MAPLE_GRAPHICS_VULKAN_GLOBAL_H