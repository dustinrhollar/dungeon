#ifndef MAPLE_VULKAN_FRONTEND
#define MAPLE_VULKAN_FRONTEND

/*
API frontend to the backend graphics.
*/

namespace vkMaple
{
    // TODO(Dustin): Per-view descriptor set
    
    
    // Startup routines
    void InitializeVulkan();
    void ShutdownVulkan();
    
    // Render a frame
    void BeginFrame();
    void EndFrame();
    
    // Initializers
    int CreatePipeline(PipelineCreateInfo info, 
                       VkDescriptorSetLayout descriptor_set_layout);
    
    void CreateDescriptorSetLayout(VkDescriptorSetLayout *descriptorset_layout, 
                                   VkDescriptorSetLayoutBinding *bindings,
                                   u32 binding_count);
    
    // Get the descriptor layout
    VkDescriptorSetLayout GetDescriptorLayout(DescritporLayoutType descriptor_type);
    
    // Creating mesh 
    void CreateMesh(CreateMeshInfo mesh_info);
    
    
    typedef struct
    {
        bool is_indexed;
        u32 mesh_id;
    } DrawMeshInfo;
    
    
    void BindVertexBuffer(u32 id, /* id of the mesh to draw */
                          u32 firstBinding = 0, /*  index of the first vertex input binding whose state is updated by the command */
                          u32 bindingCount = 1 /*  number of vertex input bindings whose state is updated by the command. */);
    void DrawIndexed(u32 mesh_id,
                     u32 instance_count,
                     u32 first_index,
                     u32 vertex_offset,
                     u32 first_instance);
    /*
    void BindDescriptorSet(DescritporLayoutType descriptor_type,
                           u32 first_set,
                           u32 descriptor_set_count,
                           u32 dynamic_offset_count = 0,
                           const u32 *p_dynamic_offsets = nullptr);
    */
    void BindPipeline(u32 pipeline_id);
    void Map(void **mapped_memory);
    void Unmap();
    
    
    typedef struct 
    {
        
    } Renderer;
} // vkMaple

#endif // MAPLE_VULKAN_FRONTEND