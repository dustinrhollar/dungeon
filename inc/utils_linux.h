// TODO(Dustin): remove in favor of platform api

#ifndef UTILS_H
#define UTILS_H

typedef struct 
{
    int size;
    char *absoluteFilename;
    char *contents;
} FileInfo;

void getAbsoluteFilepathLinux(FileInfo *info, const char *relative)
{
    info->absoluteFilename = realpath(relative, NULL);
    if (info->absoluteFilename == NULL)
    {
        fprintf(stderr, "Unable to get absolute path for %s\n", relative);
        info = NULL;
    }
}

void readShaderFile(FileInfo *info, const char *relative)
{
    getAbsoluteFilepathLinux(info, relative);
    if (info == NULL) return;
    
    // Read the file
    FILE *f = fopen(info->absoluteFilename, "r");
    if (f == NULL)
    {
        fprintf(stderr, "Could not open file \"%s\".\n", info->absoluteFilename);
        info = NULL;
        return;
    }
    
    fseek(f, 0, SEEK_END);
    size_t fsize = ftell(f);
    fseek(f, 0, SEEK_SET);
    
    info->contents = (char*)malloc(fsize + 1);
    if (info->contents == NULL)
    {
        fprintf(stderr, "Not enough memory to read \"%s\".\n", info->absoluteFilename);
        info = NULL;
        return;
    }
    
    size_t br = fread(info->contents, 1, fsize, f);
    if (br < fsize)
    {
        fprintf(stderr, "Could not read file \"%s\".\n", info->absoluteFilename);
    }
    fclose(f);
    
    info->contents[fsize] = '\0';
    info->size = fsize;
}

void freeFileInfo(FileInfo *info)
{
    free(info->absoluteFilename);
    free(info->contents);
    info->size = 0;
}

#endif