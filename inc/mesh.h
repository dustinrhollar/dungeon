#ifndef DUNGEON_MESH_H
#define DUNGEON_MESH_H

typedef struct
{
    u32 textureId;
    u32 width;
    u32 height;
    void *data;
} TextureInfo;

bool compareVec3(Vec3 first, Vec3 second)
{
    return first.x == second.x &&
        first.y == second.y &&
        first.z == second.z;
}

bool compareVec2(Vec2 first, Vec2 second)
{
    return first.x == second.x &&
        first.y == second.y;
}

struct Vertex 
{
    Vec3 pos;
    Vec3 col;
    Vec2 tex;
    
    
    bool operator==(const Vertex& other) const {
        return compareVec3(pos, other.pos) && 
            compareVec3(col, other.col) && 
            compareVec2(tex, other.tex);
    }
};

// SOURCE: https://github.com/g-truc/glm/blob/master/glm/gtx/hash.inl
void hash_combine(size_t &seed, size_t hash)
{
    hash += 0x9e3779b9 + (seed << 6) + (seed >> 2);
    seed ^= hash;
}

size_t hashVec2(Vec2 const& v)
{
    size_t seed = 0;
    std::hash<float> hasher;
    hash_combine(seed, hasher(v.x));
    hash_combine(seed, hasher(v.y));
    return seed;
}


size_t hashVec3(Vec3 const& v)
{
    size_t seed = 0;
    std::hash<float> hasher;
    hash_combine(seed, hasher(v.x));
    hash_combine(seed, hasher(v.y));
    hash_combine(seed, hasher(v.z));
    return seed;
}


namespace std {
    template<> struct hash<Vertex> {
        size_t operator()(Vertex const& vertex) const {
            return ((hashVec3(vertex.pos) ^
                     (hashVec3(vertex.col) << 1)) >> 1) ^
                (hashVec2(vertex.tex) << 1);
        }
    };
}

typedef struct 
{
	u64 key;
    u32 vertexCount;
    u32 indexCount;
    u32 textureCount;
    
    Vertex *vertices;
    u32    *indices;
    u32 *textures;
} Mesh;

#endif