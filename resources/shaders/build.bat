@echo off

C:\VulkanSDK\1.1.121.2\Bin\glslc.exe shader.vert -o vert.spv
C:\VulkanSDK\1.1.121.2\Bin\glslc.exe shader.frag -o frag.spv

C:\VulkanSDK\1.1.121.2\Bin\glslc.exe shader_simple.vert -o simple_vert.spv
C:\VulkanSDK\1.1.121.2\Bin\glslc.exe shader_simple.frag -o simple_frag.spv