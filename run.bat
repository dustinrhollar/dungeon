@echo off

SET NONE_BUILD=""
SET TERRAIN=terrain


GOTO :MAIN
EXIT /B %ERRORLEVEL%

:Terrain
	pushd build\
		terrain_gen.exe
	popd
	EXIT /B 0

:Default
	pushd build\
		dungeon.exe > log.txt
	popd
	EXIT /B 0

:MAIN
	IF "%1" == %NONE_BUILD% (
    	CALL :Default
		EXIT /B %ERRORLEVEL%
	)

	IF %1 == %TERRAIN% (
        CALL :Terrain
        EXIT /B %ERRORLEVEL%
    )