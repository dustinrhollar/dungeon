

// Set whether or not the application is runnin
global bool isRunning = false;

// Choose the graphics API to use and load interface functions
// TODO(Dustin): Better handling of filepath
const char *GAME_LIB = "build/libgame.so";
const char *GRAPHICS_LIB = "build/libvulkan.so";

typedef struct
{
    void *handle;
    ino_t id;
    GameAPI api;
} Game;

typedef struct
{
    void *handle;
    ino_t id;
    VulkanAPI api;
} Graphics;

void gameLoad(Game *game)
{
    struct stat attr;
    if ((stat(GAME_LIB, &attr) == 0) && (game->id != attr.st_ino))
    {
        if (game->handle)
        {
            // unload?
            dlclose(game->handle);
        }
        
        void *handle = dlopen(GAME_LIB, RTLD_NOW);
        if (handle)
        {
            game->handle = handle;
            game->id = attr.st_ino;
            
            const GameAPI *api = (GameAPI*)dlsym(game->handle, "GAME_API");
            if (api != NULL)
            {
                game->api = *api;
            }
            else
            {
                dlclose(game->handle);
                game->handle = NULL;
                game->id = 0;
            }
        }
        else
        {
            game->handle = NULL;
            game->id = 0;
        }
    }
}

void graphicsLoad(Graphics *graphics)
{
    struct stat attr;
    if ((stat(GRAPHICS_LIB, &attr) == 0) && (graphics->id != attr.st_ino))
    {
        if (graphics->handle)
        {
            // unload?
            dlclose(graphics->handle);
        }
        
        void *handle = dlopen(GRAPHICS_LIB, RTLD_NOW);
        if (handle)
        {
            graphics->handle = handle;
            graphics->id = attr.st_ino;
            
            const VulkanAPI *api = (VulkanAPI*)dlsym(graphics->handle, "VULKAN_API");
            if (api != NULL)
            {
                graphics->api = *api;
                
                graphics->api.init();
            }
            else
            {
                dlclose(graphics->handle);
                graphics->handle = NULL;
                graphics->id = 0;
            }
        }
        else
        {
            graphics->handle = NULL;
            graphics->id = 0;
        }
    }
}

void graphicsUnload(Graphics *graphics)
{
    if (graphics->handle) {
        graphics->api.shutdown();
        dlclose(graphics->handle);
        graphics->handle = NULL;
        graphics->id = 0;
    }
}

void gameUnload(Game *game)
{
    if (game->handle) {
        game->api.shutdown();
        dlclose(game->handle);
        game->handle = NULL;
        game->id = 0;
    }
}

// Main loop of the program
void mainLoop(Graphics *graphics, Game *game)
{
    //struct timespec start;
    //clock_gettime(CLOCK_MONOTONIC, &start);
    
    while (isRunning)
    {
        // Reload game code if necessary
        // -----------------------------------
        gameLoad(game);
        if (game->handle == NULL)
        {
            isRunning = false;
            fprintf(stderr, "Failed to load game code!\n");
        }
        
        // Reload graphics code if necessary
        // -----------------------------------
        graphicsLoad(graphics);
        if (graphics->handle == NULL)
        {
            isRunning = false;
            fprintf(stderr, "Failed to load graphics code!\n");
        }
        
        
        // Update the game state
        // -----------------------------------
        FrameInfo frame = {};
        game->api.update(&frame);
        
        // Render the scene
        // -----------------------------------
        if (frame.drawnObjects.size() > 0)
        {
            graphics->api.update(&frame);
        }
        
        // Performance Query
        //struct timespec end;
        //clock_gettime(CLOCK_MONOTONIC, &end);
        
        //u32 ms  = (end.tv_sec - start.tv_sec)*1000 + (end.tv_nsec - start.tv_nsec)/1e6;
        //u32 fps = (u32) (1.0e9 / ((end.tv_sec - start.tv_sec)*1e9 + (end.tv_nsec - start.tv_nsec)));
        // printf("Elapsed time: %d ms %d FPS\n", ms, fps);
        
        //start = end;
        
        if (graphics->api.error() || game->api.error())
            isRunning = false;
    }
}

// Shutdown running processes
void shutdownProgram(Graphics *graphics, Game *game)
{
    gameUnload(game);
    graphicsUnload(graphics);
}

int main (void)
{
    // Load graphics module
    Graphics graphics = {0};
    graphicsLoad(&graphics);
    if (graphics.api.error()) 
    {
        fprintf(stderr, "Unable to load and initialize a graphics API!\n");
        return EXIT_FAILURE;
    }
    
    // Load game module
    Game game = {0};
    gameLoad(&game);
    if (game.api.error()) 
    {
        fprintf(stderr, "Unable to load and initialize a game API!\n");
        return EXIT_FAILURE;
    }
    
    // Create game state
    FrameInfo frame;
    game.api.init(&frame);
    
    // Create/Delete/Render initial game
    graphics.api.update(&frame);
    
    isRunning = true;
    mainLoop(&graphics, &game);
    
    shutdownProgram(&graphics, &game);
    
    return EXIT_SUCCESS;
}