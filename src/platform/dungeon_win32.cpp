// GMath
#ifndef GMATH_IMPLEMENTATION
#define GMATH_IMPLEMENTATION
#endif
#ifndef GMATH_DEPTH_ZERO_TO_ONE
#define GMATH_DEPTH_ZERO_TO_ONE
#endif
#include <GMath/GMath.h>

// C/C++ libs
//------------------------------------------
#include <vector>

// CLIB
#include <stdlib.h>
#include <stdio.h>

#include <windows.h>

// glfw
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

// vulkan
#include <vulkan/vulkan.h>

// Source
//------------------------------------------
// Headers
#include <appconfig.h>
//#include <mesh.h>
//#include <frame.h>

#include <dungeon_platform.h>
#define GRAPHICS_PLATFORM_DEFINITION
#include <vulkan/vulkan_api.h>
#include <dungeon.h>

global bool g_is_running = true;

global const int WIDTH = 800;
global const int HEIGHT = 600;

// Not null terminated
global char *exe_filepath = nullptr;

void error_callback(int error, const char *description);
internal void framebufferResizeCallback(GLFWwindow *window, int width, int height);
void processInput(GLFWwindow *window);
internal void processInput(GLFWwindow *window);

struct CodeDLL
{
    HMODULE game;
} GlobalCodeDLL;

global GameCode GlobalGameCode;
global GLFWwindow *GlobalWindow;
global bool GlobalIsRunning;
global u64  GlobalPerfCountFrequency;

internal GameCode LoadGameCode(const char *libname)
{
    GameCode gc = {};
    
    GlobalCodeDLL.game = LoadLibrary(TEXT(libname));
    
    if (GlobalCodeDLL.game != NULL)
    {
        gc.Initialize = 
            (game_initialize *)GetProcAddress(GlobalCodeDLL.game, "GameInitialize");
        gc.Update     = (game_update *)GetProcAddress(GlobalCodeDLL.game, "GameUpdate");
        gc.Resize     = (game_resize *)GetProcAddress(GlobalCodeDLL.game, "GameResize");
        if (gc.Initialize && gc.Update && gc.Resize)
        {
            gc.isValid = true;
        }
    }
    
    if (!gc.isValid)
    {
        printf("Game Code was not valid\n");
        gc.Initialize = GameInitializeStub;
        gc.Update = game_update_stub;
        gc.Resize = GameResizeStub;
    }
    
    return gc;
}

internal void InitializeWindow()
{
    glfwInit();
    
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
    
    GlobalWindow = glfwCreateWindow(WIDTH, HEIGHT, "Dungeon", nullptr, nullptr);
    if (GlobalWindow == nullptr)
    {
        printf("Unable to create glfw window!\n");
        return;
    }
    
    glfwSetErrorCallback(error_callback);
    glfwSetFramebufferSizeCallback(GlobalWindow, framebufferResizeCallback);
}

internal void RenderLoop()
{
    while (GlobalIsRunning)
    {
        processInput(GlobalWindow);
        if (glfwWindowShouldClose(GlobalWindow))
        {
            GlobalIsRunning = false;
            return;
        }
        
        GlobalGameCode.Update();
        
        glfwPollEvents();
    }
}

PLATFORM_WAIT_FOR_EVENTS(PlatformWaitForEvents)
{
    int width = 0, height = 0;
    while (width == 0 || height == 0)
    {
        glfwGetFramebufferSize(GlobalWindow, &width, &height);
        glfwWaitEvents();
    }
}

// TODO(Dustin): Read file into transient memory?
PLATFORM_READ_FILE(PlatformReadFile)
{
    // Read the file
    FILE *f = fopen(filename, "rb");
    if (f == NULL)
    {
        fprintf(stderr, "Could not open file \"%s\".\n", filename);
        info->contents = NULL;
        return;
    }
    
    fseek(f, 0, SEEK_END);
    size_t fsize = ftell(f);
    fseek(f, 0, SEEK_SET);
    
    info->contents = (char*)malloc(fsize + 1);
    if (info->contents == NULL)
    {
        fprintf(stderr, "Not enough memory to read \"%s\".\n", filename);
        info->contents = NULL;
        return;
    }
    
    size_t br = fread(info->contents, 1, fsize, f);
    if (br < fsize)
    {
        fprintf(stderr, "Could not read file \"%s\".\n", filename);
        info->contents = NULL;
        return;
    }
    fclose(f);
    
    info->contents[fsize] = '\0';
    info->size = fsize;
}

// Takes a pointer to array of extensions that will be filled out.
PLATFORM_GET_REQUIRED_EXTENSIONS(GetRequiredExtensions)
{
    u32 glfwExtensionCount = 0;
    const char** glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
    
    std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);
    
    if (is_validation_enabled) {
        extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    }
    
    return extensions;
    
}

PLATFORM_GET_FRAMEBUFFER_SIZE(GetFramebufferSize)
{
    glfwGetFramebufferSize(GlobalWindow, width, height);
}

// Creates a Vulkan KHRSurface
PLATFORM_CREATE_SURFACE(CreateVulkanSurface)
{
    glfwCreateWindowSurface(instance, GlobalWindow, nullptr, surface);
}

PLATFORM_GET_WALL_CLOCK(GetWallClock)
{
    LARGE_INTEGER Result;
    QueryPerformanceCounter(&Result);
    return (Result.QuadPart);
}

PLATFORM_GET_SECONDS_ELAPSED(GetSecondsElapsed)
{
    float Result = ((float)(end - start) /
                    (float)GlobalPerfCountFrequency);
    return(Result);
}

// TODO(Dustin): Maybe not use MAX_PATH?
#define WIN32_STATE_FILE_NAME_COUNT MAX_PATH
internal void BuildExeFilepath()
{
    // Get the full filepath
    char exe[WIN32_STATE_FILE_NAME_COUNT];
    size_t filename_size = sizeof(exe);
    printf("Size of filename %zd\n", filename_size);
    
    DWORD filepath_size = GetModuleFileNameA(0, exe, filename_size);
    printf("Size of filepath %ld\n", filepath_size);
    printf("Filepath %s\n", exe);
    
    // find last "\"
    char *pos = 0;
    for (char *Scanner = exe; *Scanner; ++Scanner)
    {
        if (*Scanner == '\\')
        {
            pos = Scanner + 1;
        }
    }
    
    int len = (pos - exe) + 1;
    exe_filepath = (char*)malloc(len);
    memcpy(exe_filepath, exe, len);
    exe_filepath[len - 1] = '\0';
    
    printf("Fullpath %s\n", exe_filepath);
}

PLATFORM_GET_FULL_FILEPATH(GetFullFilepath)
{
    // TODO(Dustin): error check?
    
    int str_size = sprintf(fullpath, "%s%s", exe_filepath, filename);
    fullpath[str_size] = '\0';
    return str_size;
}

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, PWSTR pCmdLine, int nCmdShow)
{
    BuildExeFilepath();
    
    const char *game_lib = "libgame.dll";
    const char *vulkan_lib = "libvulkan.dll";
    
    LARGE_INTEGER PerfCountFrequencyResult;
    QueryPerformanceFrequency(&PerfCountFrequencyResult);
    GlobalPerfCountFrequency = PerfCountFrequencyResult.QuadPart;
    
    // Create the window.
    u64 before = GetWallClock();
    
    InitializeWindow();
    if (GlobalWindow == NULL)
        return EXIT_FAILURE;
    
    u64 after = GetWallClock();
    float elapsed = GetSecondsElapsed(before, after);
    
    printf("Time to open window %9.6fs\n", elapsed);
    
    // Load DLLs
    GlobalGameCode = LoadGameCode(game_lib);
    if (!GlobalGameCode.isValid)
        return EXIT_FAILURE;
    
    // Setup Platform API
    platform_api api;
    api.WaitForEvents         = PlatformWaitForEvents;
    api.ReadFile              = PlatformReadFile;
    api.GetRequiredExtensions = GetRequiredExtensions;
    api.GetFramebufferSize    = GetFramebufferSize;
    api.CreateSurface         = CreateVulkanSurface;
    api.GetWallClock          = GetWallClock;
    api.GetSecondsElapsed     = GetSecondsElapsed;
    api.GetFullFilepath       = GetFullFilepath;
    
    GlobalGameCode.Initialize(api);
    
    // Run the message loop.
    GlobalIsRunning = true;
    RenderLoop();
    
    return EXIT_SUCCESS;
}

void error_callback(int error, const char *description)
{
    printf("GLFW Error: %s\n", description);
}

internal void framebufferResizeCallback(GLFWwindow *window, int width, int height)
{// TODO(Dustin): Add Game API for this, or per frame information
    GlobalGameCode.Resize();
}

// Input
//----------------------------------------------
internal void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}