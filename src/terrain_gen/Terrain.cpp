//
// TODO(Dustin): 
// - Create TerrainMesh and write to file in a binary format so main program can read it at runtime
// - Generate normals for the mesh
// - Generate LODs
//
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdio>
#include <stdlib.h>
#include <stdint.h>

#define global        static
#define local_persist static
#define internal      static

typedef int8_t  i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float  real32;
typedef double real64;

#define WIREFRAME 1

#define VOXEL 0

// Define which Procedural Terrain Generation algorithm to use
#define NOISE          1
#define THERMAL        0
#define HYDRAULIC      0
#define INVERSETHERMAL 0

// Comment to print Greyscale
// when doing proceural terrain gen
#define COLOR 1


// GMath
#define GMATH_IMPLEMENTATION
#include <GMath/GMath.h>

#include "OpenSimplexNoise.h"
#include "TerrainGenerator.h"
#include "TerrainGenerator.cpp"

#include "Terrain.h"

#define EROSION_ITERATIONS 20

// a bilinear interpolation function used for finding texture coordinates
static Vec2 bilerp( int x, int y, int width, int height ) {
    
    Vec2 TL = { 0, 1 };
    Vec2 TR = { 1, 1 };
    Vec2 BL = { 0, 0 };
    Vec2 BR = { 1, 0 };
    
    float t = (float)y / (float)height;
    float s = (float)x / (float)width;
    
    Vec2 l = TL + s * ( BL - TL );
    Vec2 r = TR + s * ( BR - TR );
    
    return l + t * ( r - l );
}

// Create the heightmap using noise and erosion simulations
void CreateHeightmap( NoiseOctaveSimulation noiseSim ) {
    
    // --------------------------------------------------------------------------------------------- //
    // Simulate noise
    // --------------------------------------------------------------------------------------------- //
    std::cout << "Simulating Noise..." << std::endl;
    float * heightmap = SimulateNoise( noiseSim );
    
    // --------------------------------------------------------------------------------------------- //
    // Simulate erosion
    // --------------------------------------------------------------------------------------------- //
#if THERMAL
    std::cout << "Simulating thermal erosion..." << std::endl;
    
    // Create Thermal Erosion Simulation Struct
    ThermalErosionSimlation simulation;
    simulation.width            = noiseSim.width;
    simulation.height           = noiseSim.height;
    simulation.numberIterations = EROSION_ITERATIONS;
    simulation.noiseMap         = heightmap;
    
    // Run the simulation
    SimulateThermalErosion( simulation );
    //#endif
    
#elif HYDRAULIC
    std::cout << "Simulating hydraulic erosion..." << std::endl;
    
    // Set up constants for Erosion coefficients and struct
    float kr = 0.01f;           // Rain constant
    float ks = 0.01f;           // solubility constant of the terrain
    float ke = 0.5f;            // evaporation coefficient
    float kc = 0.01f;           // sediment transfer maximum coefficient
    
    ErosionCoefficient coeffErosion;
    coeffErosion.Kr = kr;
    coeffErosion.Ks = ks;
    coeffErosion.Ke = ke;
    coeffErosion.Kc = kc;
    
    // Create Hydraulic Erosion Simulation Struct
    HydraulicErosionSimulation simulation;
    simulation.erosionCoeffStruct = &coeffErosion;
    simulation.width              = noiseSim.width;
    simulation.height             = noiseSim.height;
    simulation.noiseMap           = heightmap;
    simulation.numberIterations   = EROSION_ITERATIONS;
    
    // Run the simulation
    SimulateHydraulicErosion( simulation );
    //#endif
    
#elif INVERSETHERMAL
    std::cout << "Simulating inverse thermal erosion..." << std::endl;
    
    // Create Thermal Erosion Simulation Struct
    ThermalErosionSimlation simulation;
    simulation.width            = noiseSim.width;
    simulation.height           = noiseSim.height;
    simulation.numberIterations = EROSION_ITERATIONS;
    simulation.noiseMap         = heightmap;
    
    // Run the simulation
    SimulateInverseThermalErosion( simulation );
#endif
    
    // --------------------------------------------------------------------------------------------- //
    // Write to the ppm file
    // --------------------------------------------------------------------------------------------- //
    std::cout << "Writing to image file..." << std::endl;
    std::ofstream ofs;
    
    // Filename buffer
    char buff[22];
    
    // Define the name of the output file
#if NOISE
    std::cout << "Writing to noise image file..." << std::endl;
#if COLOR
    sprintf( buff, "resources/terrain/noiseC%d.ppm", noiseSim.numOctaves );
    ofs.open( buff, std::ios::out | std::ios::binary );
#else
    sprintf( buff, "resources/terrain/noiseG%d.ppm", noiseSim.numOctaves );
    ofs.open( buff, std::ios::out | std::ios::binary );
#endif
    //#endif
    
#elif THERMAL
    std::cout << "Writing to thermal image file..." << std::endl;
#if COLOR
    sprintf( buff, "resources/terrain/thermC%d.ppm", numberIterations );
    ofs.open( buff, std::ios::out | std::ios::binary );
#else
    sprintf( buff, "resources/terrain/thermG%d.ppm", numberIterations );
    ofs.open( buff, std::ios::out | std::ios::binary );
#endif
    //#endif
    
#elif HYDRAULIC
#ifdef COLOR
    sprintf( buff, "resources/terrain/hydroC%d.ppm", numberIterations );
    ofs.open( buff, std::ios::out | std::ios::binary );
#else
    sprintf( buff, "resources/terrain/hydroG%d.ppm", numberIterations );
    ofs.open( buff, std::ios::out | std::ios::binary );
#endif
    //#endif
    
#elif INVERSETHERMAL
#ifdef COLOR
    sprintf( buff, "resources/terrain/invThC%3d.ppm", numberIterations );
    ofs.open( buff, std::ios::out | std::ios::binary );
#else
    sprintf( buff, "resources/terrain/invThG%3d.ppm", numberIterations );
    ofs.open( buff, std::ios::out | std::ios::binary );
#endif
#endif
    
    ofs << "P6\n" << noiseSim.width << " " << noiseSim.height << "\n255\n";
    int length = noiseSim.width * noiseSim.height;
    for (int k = 0; k < length; ++k) {
        
#if COLOR
        BiomeColor bc;
        int biomereturn = GetBiome( heightmap[ k ] );
        
        if ( biomereturn == WATER ) {
            ofs << bc.water[0] << bc.water[1] << bc.water[2];
        }
        else if ( biomereturn == BEACH ) {
            ofs << bc.beach[0] << bc.beach[1] << bc.beach[2];
        }
        else if ( biomereturn == FOREST ) {
            ofs << bc.forest[0] << bc.forest[1] << bc.forest[2];
        }
        else if ( biomereturn == JUNGLE ) {
            ofs << bc.jungle[0] << bc.jungle[1] << bc.jungle[2];
        }
        else if ( biomereturn == SAVANNAH ) {
            ofs << bc.savannah[0] << bc.savannah[1] << bc.savannah[2];
        }
        else if ( biomereturn == DESERT ) {
            ofs << bc.desert[0] << bc.desert[1] << bc.desert[2];
        }
        else if ( biomereturn == SNOW ) {
            ofs << bc.snow[0] << bc.snow[1] << bc.snow[2];
        }
        else { /* this better not ever happen */ }
        
#else
        // Print greyscale
        unsigned char n = heightmap[ k ] * 255;
        ofs << n << n << n;
#endif
    }
    ofs.close();
    
    std::cout << "Completed terrain simulation." << std::endl;
}

// TODO(Dustin): Rather than writing a heightmap to a file, write the generated mesh.
/*
// Create the terrain mesh
void CreateBaseMesh() {
    // set up vertex data (and buffer(s)) and configure vertex attributes
    // ------------------------------------------------------------------
    // 6 : verts per quad, 3 : 3 data points per vert, 2 : 2 texture coords
    size = terrainWidth * terrainHeight * 6 * 6;
    terrainMesh = new float[ size ];
    
    int idx = 0;
    for (unsigned int i = 0; i < terrainWidth; ++i ) {
        for(unsigned int j = 0; j < terrainHeight; ++j ) {
        
            // Perform bi-linear interpolation to the the texture coordinate of each corner of the quad
            glm::vec2 TR = bilerp( i    , j + 1, terrainWidth, terrainHeight );
            glm::vec2 BR = bilerp( i + 1, j + 1, terrainWidth, terrainHeight );
            glm::vec2 TL = bilerp( i    , j    , terrainWidth, terrainHeight );
            glm::vec2 BL = bilerp( i + 1, j    , terrainWidth, terrainHeight );
            
            // TRIANGLE 1
            // Vertex 1, TR: i,j + 1
            terrainMesh[ idx++ ] = (i);// / height;
            terrainMesh[ idx++ ] = (j + 1);// / width;
            //terrainMesh[ idx++ ] = (0);
            terrainMesh[ idx++ ] = (TR[0]);
            terrainMesh[ idx++ ] = (TR[1]);
            terrainMesh[ idx++ ] = 1.0f;
            terrainMesh[ idx++ ] = 1.0f;
            
            // Vertex 2, BR: i + 1, j + 1
            terrainMesh[ idx++ ] = (i + 1);// / height;
            terrainMesh[ idx++ ] = (j + 1);// / width;
            //terrainMesh[ idx++ ] = (0);
            terrainMesh[ idx++ ] = BR[0];
            terrainMesh[ idx++ ] = BR[1];
            terrainMesh[ idx++ ] = 1.0f;
            terrainMesh[ idx++ ] = 0.0f;
            
            // Vertex 3, TL: i, j
            terrainMesh[ idx++ ] = (i);// / height;
            terrainMesh[ idx++ ] = (j);// / width;
            //terrainMesh[ idx++ ] =  0;
            terrainMesh[ idx++ ] = TL[0];
            terrainMesh[ idx++ ] = TL[1];
            terrainMesh[ idx++ ] = 0.0f;
            terrainMesh[ idx++ ] = 1.0f;
            
            // Triangle 2
            // Vertex 1, BR: i + 1, j + 1
            terrainMesh[ idx++ ] = (i + 1);// / height;
            terrainMesh[ idx++ ] = (j + 1);// / width;
            //terrainMesh[ idx++ ] = 0;
            terrainMesh[ idx++ ] = BR[0];
            terrainMesh[ idx++ ] = BR[1];
            terrainMesh[ idx++ ] = 1.0f;
            terrainMesh[ idx++ ] = 0.0f;
            
            // Vertex 2, BL: i + 1, j
            terrainMesh[ idx++ ] = (i + 1);// / height;
            terrainMesh[ idx++ ] = (j);// / width;
            //terrainMesh[ idx++ ] = 0;
            terrainMesh[ idx++ ] = BL[0];
            terrainMesh[ idx++ ] =  BL[1];
            terrainMesh[ idx++ ] = 0.0f;
            terrainMesh[ idx++ ] = 0.0f;
            
            // Vertex 3, TL: i, j
            terrainMesh[ idx++ ] = (i);// / height;
            terrainMesh[ idx++ ] = (j);// / width;
            //terrainMesh[ idx++ ] = 0;
            terrainMesh[ idx++ ] = TL[0];
            terrainMesh[ idx++ ] = TL[1];
            terrainMesh[ idx++ ] = 0.0f;
            terrainMesh[ idx++ ] = 1.0f;
        }
    }
}
*/

// TODO(Dustin): Add user input to configure terrain
int main(void)
{
    // Resolution of the generated heightmap texture
    int textureWidth  = 512;
    int textureHeight = 512;
    
    // height scale
    // 256x256, 40
    u32 scale = 40;
    
    // Set up noise simulation
    SimplexNoise::NoiseOctaveSimulation noiseSim;
    noiseSim.width         = textureWidth;             // Width of the texture
    noiseSim.height        = textureHeight;            // Height of the texture
    noiseSim.numOctaves    = 8;                        // Control the number of octaves used
    noiseSim.persistence   = 0.50f;                    // Control the roughness of the output
    noiseSim.low           = 0.00f;                    // Low output value
    noiseSim.high          = 1.00f;                    // Height output value
    noiseSim.exp           = 2.00f;                    // Controls the intensity of black to white
    noiseSim.dim           = noiseSim.TWODIMENSION;    // Define the dimension of noise
    
    // Size of a terrain chunk
    u32 terrainWidth  = 256;
    u32 terrainHeight = 256;
    
    CreateHeightmap( noiseSim );
}