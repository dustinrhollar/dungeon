using namespace vkMaple;
using namespace vkMaple::Base;

void VulkanDevice::Initialize()
{
    CreateInstance();
    SetupDebugMessenger();
    Platform.CreateSurface(&surface, instance);
    
    PickPhysicalDevice();
    CreateLogicalDevice();
    CreateCommandPool();
    CreateSyncObjects();
    
    // Get the physical device limits
    VkPhysicalDeviceProperties properties;
    vkGetPhysicalDeviceProperties(physical_device, &properties);
    
    limits = properties.limits;
    printf("minUniformBufferOffsetAlignment: %lld\n", limits.minUniformBufferOffsetAlignment);
    
}
void VulkanDevice::Shutdown()
{
    for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
    {
        vkDestroySemaphore(device, sync_objects.render_finished_semaphore[i], nullptr);
        vkDestroySemaphore(device, sync_objects.image_available_semaphore[i], nullptr);
        vkDestroyFence(device, sync_objects.in_flight_fences[i], nullptr);
    }
    
    vkDestroyCommandPool(device, command_pool, nullptr);
    
    vkDestroyDevice(device, nullptr);
    if (Validation::enabled_validation_layers) {
        DestroyDebugUtilsMessengerEXT(debug_messenger, nullptr);
    }
    
    vkDestroySurfaceKHR(instance, surface, nullptr);
    vkDestroyInstance(instance, nullptr);
}

VkFormat VulkanDevice::FindDepthFormat()
{
    // Ordered from most to least desirable
    VkFormat candidates[] = {
        VK_FORMAT_D32_SFLOAT, 
        VK_FORMAT_D32_SFLOAT_S8_UINT, 
        VK_FORMAT_D24_UNORM_S8_UINT
    };
    VkImageTiling tiling = VK_IMAGE_TILING_OPTIMAL;
    VkFormatFeatureFlags features = VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT;
    
    VkFormat depthFormat = VK_FORMAT_UNDEFINED;
    for (u32 i = 0; i < sizeof(candidates)/sizeof(candidates[0]); ++i) {
        VkFormat format = candidates[i];
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties(physical_device, format, &props);
        
        if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
            depthFormat = format;
            break;
        } else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
            depthFormat = format;
            break;
        }
    }
    
    return depthFormat;
}

void VulkanDevice::CreateRenderPass(VkRenderPass *render_pass, 
                                    VkFormat swapchain_image_format)
{
    
    VkAttachmentDescription colorAttachment = {};
    colorAttachment.format = swapchain_image_format;
    colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    
    VkAttachmentReference colorAttachmentRef = {};
    colorAttachmentRef.attachment = 0;
    colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    
    VkAttachmentDescription depthAttachment = {};
    depthAttachment.format = FindDepthFormat();
    depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    
    VkAttachmentReference depthAttachmentRef = {};
    depthAttachmentRef.attachment = 1;
    depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    
    
    VkSubpassDescription subpass = {};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;
    subpass.pDepthStencilAttachment = &depthAttachmentRef;
    
    VkSubpassDependency dependency = {};
    dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependency.dstSubpass = 0;
    dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.srcAccessMask = 0;
    dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    
    VkAttachmentDescription attachments[2] = {
        colorAttachment,
        depthAttachment
    };
    
    VkRenderPassCreateInfo renderPassInfo = {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = 2;
    renderPassInfo.pAttachments = attachments;
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;
    renderPassInfo.dependencyCount = 1;
    renderPassInfo.pDependencies = &dependency;
    
    VK_CHECK_RESULT(vkCreateRenderPass(device, &renderPassInfo, nullptr, render_pass), 
                    "Failed to create render pass!");
}

void VulkanDevice::CreateCommandbuffer(VkCommandBuffer *buffers, u32 buffer_count)
{
    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = command_pool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = buffer_count;
    
    VK_CHECK_RESULT(vkAllocateCommandBuffers(device, &allocInfo, buffers),
                    "Failed to allocate command buffers!");
    
}


VkCommandBuffer VulkanDevice::BeginSingleTimeCommands()
{
    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = command_pool;
    allocInfo.commandBufferCount = 1;
    
    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);
    
    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    
    vkBeginCommandBuffer(commandBuffer, &beginInfo);
    
    return commandBuffer;
}

void VulkanDevice::EndSingleTimeCommands(VkCommandBuffer commandBuffer)
{
    vkEndCommandBuffer(commandBuffer);
    
    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;
    
    vkQueueSubmit(queue_families.graphics_queue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(queue_families.graphics_queue);
    
    vkFreeCommandBuffers(device, command_pool, 1, &commandBuffer);
}

u32 VulkanDevice::FindMemoryType(u32 typeFilter, VkMemoryPropertyFlags properties)
{
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(physical_device, &memProperties);
    
    for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
        if (typeFilter & (1 << i) && 
            (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
            return i;
        }
    }
    
    SET_ERROR("failed to find suitable memory type!");
    return -1;
}

void VulkanDevice::CreateImage(u32 width, u32 height, VkFormat format, 
                               VkImageTiling tiling, VkImageUsageFlags usage, 
                               VkMemoryPropertyFlags properties, VkImage &image, 
                               VkDeviceMemory &imageMemory)
{
    
    VkImageCreateInfo imageInfo = {};
    imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageInfo.imageType = VK_IMAGE_TYPE_2D;
    imageInfo.extent.width = (u32)(width);
    imageInfo.extent.height = (u32)(height);
    imageInfo.extent.depth = 1;
    imageInfo.mipLevels = 1;
    imageInfo.arrayLayers = 1;
    imageInfo.format = format;
    imageInfo.tiling = tiling;
    imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageInfo.usage = usage;
    imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    imageInfo.flags = 0; // Optional
    
    //VK_CHECK_RESULT(vkCreateImage(device, &imageInfo, nullptr, &image),
    //"Failed to create image!");
    
    if (vkCreateImage(device, &imageInfo, nullptr, &image) != VK_SUCCESS)
    {
        SET_ERROR_RET("Failed to create image!\n");
    }
    
    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(device, image, &memRequirements);
    
    VkMemoryAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = FindMemoryType(memRequirements.memoryTypeBits, 
                                               VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
    
    //VK_CHECK_RESULT(vkAllocateMemory(device, &allocInfo, nullptr, &imageMemory),
    //"Failed to allocate image memory!");
    
    if (vkAllocateMemory(device, &allocInfo, nullptr, &imageMemory) != VK_SUCCESS)
    {
        SET_ERROR_RET("Failed to allocate image memory!\n");
    }
    
    
    vkBindImageMemory(device, image, imageMemory, 0);
}

VkImageView VulkanDevice::CreateImageView(VkImage image, VkFormat format,
                                          VkImageAspectFlags aspectFlags)
{
    VkImageViewCreateInfo viewInfo = {};
    viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    viewInfo.image = image;
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    viewInfo.format = format;
    viewInfo.subresourceRange.aspectMask = aspectFlags;
    viewInfo.subresourceRange.baseMipLevel = 0;
    viewInfo.subresourceRange.levelCount = 1;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount = 1; 
    
    VkImageView imageView;
    if (vkCreateImageView(device, &viewInfo, nullptr, &imageView) != VK_SUCCESS) {
        SET_ERROR("Failed to create texture image view!");
        return NULL;
    }
    
    return imageView;
}

void VulkanDevice::TransitionImageLayout(VkImage image, VkFormat format, 
                                         VkImageLayout oldLayout, VkImageLayout newLayout)
{
    VkCommandBuffer commandBuffer = BeginSingleTimeCommands();
    
    VkImageMemoryBarrier barrier = {};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.oldLayout = oldLayout;
    barrier.newLayout = newLayout;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = 1;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;
    barrier.srcAccessMask = 0; // TODO
    barrier.dstAccessMask = 0; // TODO
    
    if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
        
        if (format == VK_FORMAT_D32_SFLOAT_S8_UINT || 
            format == VK_FORMAT_D24_UNORM_S8_UINT) {
            barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
        }
    } 
    else {
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    }
    
    
    VkPipelineStageFlags sourceStage;
    VkPipelineStageFlags destinationStage;
    
    if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && 
        newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) 
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        
        sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && 
               newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) 
    {
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
        
        sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    } else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && 
               newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) 
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        
        sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    }
    else 
    {
        SET_ERROR_RET("Unsupported layout transition!");
    }
    
    vkCmdPipelineBarrier(
        commandBuffer,
        sourceStage, destinationStage,
        0,
        0, nullptr,
        0, nullptr,
        1, &barrier
        );
    
    EndSingleTimeCommands(commandBuffer);
}

// Swapchain Creation
void VulkanDevice::CreateSwapchain(VkSwapchainKHR &swapchain,
                                   VkFormat       &swapchain_image_format,
                                   VkExtent2D     &swapchain_extent,
                                   VkImage        *swapchain_images,
                                   VkImageView    *swapchain_image_views)
{
    
    // Query Swapchain support
    SwapChainSupportDetails details = QuerySwapchainSupport(physical_device);
    
    // Get the surface format
    VkSurfaceFormatKHR surface_format;
    {
        VkSurfaceFormatKHR *availableFormats = details.formats;
        bool found = false;
        for (int i = 0; i < arrlen(availableFormats); ++i)
        {
            VkSurfaceFormatKHR availableFormat = availableFormats[i];
            if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM &&
                availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
            {
                surface_format =  availableFormat;
                found = true;
                break;
            }
        }
        
        if (!found)
            surface_format = availableFormats[0];
    }
    
    // Get the present mode
    VkPresentModeKHR present_mode;
    {
        VkPresentModeKHR *availablePresentModes = details.presentModes;
        bool found = false;
        for (int i = 0; i < arrlen(availablePresentModes); ++i)
        {
            VkPresentModeKHR availablePresentMode = availablePresentModes[i];
            
            if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR)
            {
                present_mode = availablePresentMode;
                found = true;
                break;
            }
        }
        
        if (!found)
            present_mode = VK_PRESENT_MODE_FIFO_KHR;
    }
    
    // Get the window extent
    VkExtent2D extent;
    {
        VkSurfaceCapabilitiesKHR *capabilities = &details.capabilities;
        if (capabilities->currentExtent.width != UINT32_MAX)
        {
            extent = capabilities->currentExtent;
        }
        else
        {
            int width, height;
            Platform.GetFramebufferSize(&width, &height);
            VkExtent2D actualExtent = {(u32)width, (u32)height};
            
#define MAX(a, b) ((a) > (b)) ? a : b
#define MIN(a, b) ((a) < (b)) ? a : b
            actualExtent.width = MAX(capabilities->minImageExtent.width,
                                     MIN(capabilities->maxImageExtent.width, actualExtent.width));
            actualExtent.height = MAX(capabilities->minImageExtent.height,
                                      MIN(capabilities->maxImageExtent.height, actualExtent.height));
#undef MAX
#undef MIN
            
            extent = actualExtent;
        }
    }
    
    swapchain_image_format = surface_format.format;
    swapchain_extent = extent;
    
    u32 imageCount = details.capabilities.minImageCount + 1;
    if (details.capabilities.maxImageCount > 0 &&
        imageCount > details.capabilities.maxImageCount)
    {
        imageCount = details.capabilities.maxImageCount;
    }
    
    VkSwapchainCreateInfoKHR createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = surface;
    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = surface_format.format;
    createInfo.imageColorSpace = surface_format.colorSpace;
    createInfo.imageExtent = extent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    
    QueueFamilyIndices indices = FindQueueFamilies(physical_device);
    u32 queueFamilyIndices[] = {
        indices.graphicsFamily.value(),
        indices.presentFamily.value()};
    
    if (indices.graphicsFamily != indices.presentFamily)
    {
        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices = queueFamilyIndices;
    }
    else
    {
        createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        createInfo.queueFamilyIndexCount = 0;     // Optional
        createInfo.pQueueFamilyIndices = nullptr; // Optional
    }
    
    createInfo.preTransform = details.capabilities.currentTransform;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    createInfo.presentMode = present_mode;
    createInfo.clipped = VK_TRUE;
    createInfo.oldSwapchain = VK_NULL_HANDLE;
    
    VK_CHECK_RESULT(vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapchain),
                    "Failed to create the swap chain!");
    
    vkGetSwapchainImagesKHR(device, swapchain, &imageCount, nullptr);
    
    arrsetlen(swapchain_images, imageCount);
    vkGetSwapchainImagesKHR(device, swapchain, &imageCount, swapchain_images);
    
    // Create the image views
    arrsetlen(swapchain_image_views, (u32)arrlen(swapchain_images));
    
    for (u32 i = 0; i < arrlen(swapchain_image_views); ++i)
    {
        swapchain_image_views[i] = CreateImageView(swapchain_images[i], swapchain_image_format,
                                                   VK_IMAGE_ASPECT_COLOR_BIT);
    }
    
}

void VulkanDevice::CreateFramebuffer(VkFramebufferCreateInfo framebufferInfo, VkFramebuffer *swapchain_framebuffer)
{
    VK_CHECK_RESULT(vkCreateFramebuffer(device, &framebufferInfo, nullptr, swapchain_framebuffer),
                    "Failed to create framebuffer!");
}

void VulkanDevice::CreateCommandBuffers()
{
    
}

// Render pass creation
void VulkanDevice::CreateRenderPass(VkRenderPass & render_pass, VkFormat image_format)
{
    
}

// Buffer creation


// Image creation


// Pipeline creation



static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
VkDebugUtilsMessageTypeFlagsEXT messageType,
const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
void* pUserData
) {
    
    printf("%s\n", pCallbackData->pMessage);
    return VK_FALSE;
}

VkResult VulkanDevice::CreateDebugUtilsMessengerEXT(const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, 
                                                    const VkAllocationCallbacks* pAllocator, 
                                                    VkDebugUtilsMessengerEXT* pDebugMessenger) {
    auto func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
    if (func != nullptr) {
        return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
    } else {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

void VulkanDevice::DestroyDebugUtilsMessengerEXT(VkDebugUtilsMessengerEXT debugMessenger, 
                                                 const VkAllocationCallbacks* pAllocator) {
    auto func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
    if (func != nullptr) {
        func(instance, debugMessenger, pAllocator);
    }
}

void VulkanDevice::PopulateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo) {
    createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | 
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | 
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | 
        VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | 
        VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    createInfo.pfnUserCallback = debugCallback;
}

void VulkanDevice::SetupDebugMessenger()
{
    if (!Validation::enabled_validation_layers) return;
    
    VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
    PopulateDebugMessengerCreateInfo(createInfo);
    
    VK_CHECK_RESULT(CreateDebugUtilsMessengerEXT(&createInfo, nullptr, &debug_messenger), 
                    "Failed to set up debug messenger!");
}


void VulkanDevice::CreateInstance()
{
    if (Validation::enabled_validation_layers && !Validation::CheckValidationLayerSupport())
    {
        SET_ERROR_RET("Validation layers requested, but not available!");
    }
    
    VkApplicationInfo appInfo  = {};
    appInfo.sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName   = Globals::app_name;
    appInfo.applicationVersion = VK_MAKE_VERSION(Globals::app_version[0], 
                                                 Globals::app_version[1], 
                                                 Globals::app_version[2]);
    appInfo.pEngineName        = Globals::engine_name;
    appInfo.engineVersion      = VK_MAKE_VERSION(Globals::engine_version[0], 
                                                 Globals::engine_version[1],
                                                 Globals::engine_version[2]);
    appInfo.apiVersion         = VK_API_VERSION_1_0;
    
    VkInstanceCreateInfo createInfo = {};
    createInfo.sType                = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo     = &appInfo;
    
    // TODO(Dustin): Moveto using an array rather than a vector
    // Vulkan creates an extension interface to interact with the window api
    // glfw has a handy way of obtaining the extensions for the platform
    //std::vector<const char *> exts = Platform.GetRequiredExtensions(Validation::enabled_validation_layers);
    std::vector<const char *> exts = Platform.GetRequiredExtensions(Validation::enabled_validation_layers);
    
    createInfo.enabledExtensionCount   = (u32)exts.size();
    createInfo.ppEnabledExtensionNames = exts.data();
    
    VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo;
    if (Validation::enabled_validation_layers) {
        createInfo.enabledLayerCount   = Validation::validation_count;
        createInfo.ppEnabledLayerNames = Validation::validation_layers;
        
        PopulateDebugMessengerCreateInfo(debugCreateInfo);
        createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT*) &debugCreateInfo;
    } else {
        createInfo.enabledLayerCount = 0;
        
        createInfo.pNext = nullptr;
    }
    
    VK_CHECK_RESULT(vkCreateInstance(&createInfo, nullptr, &instance), 
                    "Failed to create instance!");
}

void VulkanDevice::PickPhysicalDevice()
{
    u32 deviceCount = 0;
    vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);
    
    if (deviceCount == 0)
    {
        printf("Failed to find a GPU with Vulkan support!\n");
        return;
    }
    
    VkPhysicalDevice *devices = nullptr;
    arrsetlen(devices, deviceCount);
    vkEnumeratePhysicalDevices(instance, &deviceCount, devices);
    
    // Finds the first suitable device
    for (u32 i = 0; i < deviceCount; ++i)
    {
        VkPhysicalDevice device = devices[i];
        
        if (IsDeviceSuitable(device))
        {
            physical_device = device;
            break;
        }
    }
    
    if (physical_device == VK_NULL_HANDLE)
    {
        printf("Failed to find a suitable GPU!\n");
        return;
    }
}

void VulkanDevice::CreateLogicalDevice()
{
    QueueFamilyIndices indices = FindQueueFamilies(physical_device);
    
    // TODO(Dustin): Remove set
    std::set<u32> uniqueQueueFamilies = { 
        indices.graphicsFamily.value(), 
        indices.presentFamily.value() 
    }; 
    
    VkDeviceQueueCreateInfo *queueCreateInfos = nullptr;
    
    float queuePriority = 1.0f;
    for (u32 queueFamily : uniqueQueueFamilies)
    {
        VkDeviceQueueCreateInfo queueCreateInfo = {};
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = queueFamily;
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &queuePriority;
        arrput(queueCreateInfos, queueCreateInfo);
    }
    
    VkPhysicalDeviceFeatures deviceFeatures = {};
    deviceFeatures.samplerAnisotropy = VK_TRUE;
    
    VkDeviceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.pQueueCreateInfos = queueCreateInfos;
    createInfo.queueCreateInfoCount = arrlen(queueCreateInfos);
    createInfo.pEnabledFeatures = &deviceFeatures;
    
    // enable the swap chain
    createInfo.enabledExtensionCount = device_extensions_count;
    createInfo.ppEnabledExtensionNames = device_extensions;
    
    // enable validation layers
    if (Validation::enabled_validation_layers) {
        createInfo.enabledLayerCount   = Validation::validation_count;
        createInfo.ppEnabledLayerNames = Validation::validation_layers;
    } else {
        createInfo.enabledLayerCount = 0;
    }
    
    VK_CHECK_RESULT(vkCreateDevice(physical_device, &createInfo, nullptr, &device), 
                    "Failed to logical device!");
    
    vkGetDeviceQueue(device, indices.graphicsFamily.value(), 0, &queue_families.graphics_queue);
    vkGetDeviceQueue(device, indices.presentFamily.value(), 0, &queue_families.present_queue);
}

VulkanDevice::QueueFamilyIndices VulkanDevice::FindQueueFamilies(VkPhysicalDevice physical_device)
{
    QueueFamilyIndices indices;
    
    u32 queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queueFamilyCount, nullptr);
    
    VkQueueFamilyProperties *queueFamilies = nullptr;
    arrsetlen(queueFamilies, queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queueFamilyCount, queueFamilies);
    
    // Find a familiy with the graphics bit
    for (u32 i = 0; i < queueFamilyCount; ++i)
    {
        VkQueueFamilyProperties queueFamily = queueFamilies[i];  
        
        if (queueFamily.queueCount > 0 &&
            queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
            indices.graphicsFamily = i;
        }
        
        VkBool32 presentSupport = false;
        vkGetPhysicalDeviceSurfaceSupportKHR(physical_device, i, surface, &presentSupport);
        if (queueFamily.queueCount > 0 && presentSupport)
        {
            indices.presentFamily = i;
        }
        
        if (indices.isComplete())
        {
            break;
        }
    }
    
    return indices;
}


bool VulkanDevice::CheckDeviceExtensionSupport(VkPhysicalDevice physical_device) 
{
    u32 extensionCount;
    vkEnumerateDeviceExtensionProperties(physical_device, nullptr, &extensionCount, nullptr);
    
    VkExtensionProperties *availableExtensions = nullptr;
    arrsetlen(availableExtensions, extensionCount);
    vkEnumerateDeviceExtensionProperties(physical_device, nullptr, &extensionCount, availableExtensions);
    
    // insert extensions into a set to make sure they are all unique
    std::set<std::string> requiredExtensions;
    for (u32 i = 0; i < device_extensions_count; ++i)
        requiredExtensions.insert(device_extensions[i]);
    
    for (int i = 0; i < arrlen(availableExtensions); ++i)
    {
        VkExtensionProperties extension = availableExtensions[i];
        requiredExtensions.erase(extension.extensionName);
    }
    
    return requiredExtensions.empty();
}

VulkanDevice::SwapChainSupportDetails VulkanDevice::QuerySwapchainSupport(VkPhysicalDevice physical_device)
{
    SwapChainSupportDetails details;
    
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, surface, &details.capabilities);
    
    u32 formatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &formatCount, nullptr);
    
    if (formatCount != 0)
    {
        arrsetlen(details.formats, formatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &formatCount, details.formats);
    }
    
    u32 presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &presentModeCount, nullptr);
    
    if (presentModeCount != 0)
    {
        arrsetlen(details.presentModes, presentModeCount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &presentModeCount, details.presentModes);
    }
    
    return details;
}

bool VulkanDevice::IsDeviceSuitable(VkPhysicalDevice physical_device)
{
    QueueFamilyIndices indices = FindQueueFamilies(physical_device);
    if (!indices.isComplete()) return false;
    
    bool extensionsSupported = CheckDeviceExtensionSupport(physical_device);
    if (!extensionsSupported) return false;
    
    // Query Swapchain support
    SwapChainSupportDetails details = QuerySwapchainSupport(physical_device);
    
    if (arrlen(details.formats) != 0 && 
        (arrlen(details.presentModes) != 0)) 
        return true;
    
    VkPhysicalDeviceFeatures supportedFeatures;
    vkGetPhysicalDeviceFeatures(physical_device, &supportedFeatures);
    return (supportedFeatures.samplerAnisotropy);
}


void VulkanDevice::CreateCommandPool()
{
    QueueFamilyIndices queueFamilyIndices = FindQueueFamilies(physical_device);
    
    VkCommandPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily.value();
    poolInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT; // Optional
    
    VK_CHECK_RESULT(vkCreateCommandPool(device, &poolInfo, nullptr, &command_pool), "Failed to create command pool!");
}

void VulkanDevice::CreateSyncObjects()
{
    arrsetlen(sync_objects.image_available_semaphore, (MAX_FRAMES_IN_FLIGHT));
    arrsetlen(sync_objects.render_finished_semaphore, (MAX_FRAMES_IN_FLIGHT));
    arrsetlen(sync_objects.in_flight_fences,          (MAX_FRAMES_IN_FLIGHT));
    
    VkSemaphoreCreateInfo semaphoreInfo = {};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    
    VkFenceCreateInfo fenceInfo = {};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
    
    for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
    {
        VK_CHECK_RESULT(vkCreateSemaphore(device, &semaphoreInfo, nullptr, &sync_objects.image_available_semaphore[i]),
                        "Failed to create synchronization objects for a frame!");
        VK_CHECK_RESULT(vkCreateSemaphore(device, &semaphoreInfo, nullptr, &sync_objects.render_finished_semaphore[i]),
                        "Failed to create synchronization objects for a frame!");
        VK_CHECK_RESULT(vkCreateFence(device, &fenceInfo, nullptr, &sync_objects.in_flight_fences[i]),
                        "Failed to create synchronization objects for a frame!");
    }
}

VkShaderModule VulkanDevice::CreateShaderModule(const char *code, int size)
{
    VkShaderModuleCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = size;
    createInfo.pCode = (u32*)code;
    
    VkShaderModule shaderModule;
    VK_CHECK_RESULT(vkCreateShaderModule(device, &createInfo, nullptr, &shaderModule), 
                    "Failed to create Shader module!\n");
    
    return shaderModule;
}

void VulkanDevice::DestroyShaderModule(VkShaderModule module)
{
    vkDestroyShaderModule(device, module, nullptr);
}

void VulkanDevice::CreatePipelineLayout(VkPipelineLayoutCreateInfo *create_info, VkPipelineLayout *layout)
{
    VK_CHECK_RESULT(vkCreatePipelineLayout(device, create_info, nullptr, layout), 
                    "Failed to create pipeline layout!");
}

void VulkanDevice::CreateGraphicsPipeline(VkGraphicsPipelineCreateInfo *pipeline_info, 
                                          VkPipeline *pipeline)
{
    VK_CHECK_RESULT(vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, pipeline_info, nullptr, pipeline),
                    "Failed to create graphics pipeline!");
}

void VulkanDevice::CreateDescriptorSetLayout(VkDescriptorSetLayout *descriptorset_layout, 
                                             VkDescriptorSetLayoutBinding *bindings,
                                             u32 binding_count)
{
    VkDescriptorSetLayoutCreateInfo layoutInfo = {};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = binding_count;
    layoutInfo.pBindings = bindings;
    
    VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device, 
                                                &layoutInfo, 
                                                nullptr, 
                                                descriptorset_layout),
                    "Failed to create descriptor set layout!");
}


void VulkanDevice::CreateDescriptorPool(VkDescriptorPool *descriptor_pool, 
                                        VkDescriptorPoolSize *pool_sizes,
                                        u32 pool_size_count,
                                        u32 swapchain_count)
{
    VkDescriptorPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount = pool_size_count;
    poolInfo.pPoolSizes = pool_sizes;
    poolInfo.maxSets = swapchain_count;
    
    VK_CHECK_RESULT(vkCreateDescriptorPool(device, &poolInfo, nullptr, descriptor_pool),
                    "Failed to create descriptor pool!");
}

void VulkanDevice::CreateDescriptorSets(VkDescriptorSet *sets,
                                        VkDescriptorSetAllocateInfo allocInfo)
{
    VK_CHECK_RESULT(vkAllocateDescriptorSets(device, 
                                             &allocInfo, 
                                             sets),
                    "Failed to allocate descriptor sets!");
}

void VulkanDevice::UpdateDescriptorSets(VkWriteDescriptorSet *descriptor_set_writes,
                                        u32 write_count,
                                        u32 copy_count,
                                        VkCopyDescriptorSet* pstop)
{
    const VkCopyDescriptorSet* stop = pstop; 
    
    vkUpdateDescriptorSets(device, 
                           write_count, 
                           descriptor_set_writes, 
                           copy_count,
                           stop);
}

void VulkanDevice::CreateBuffer(VkDeviceSize          size, 
                                VkBufferUsageFlags    usage, 
                                VkMemoryPropertyFlags properties, 
                                VkBuffer              &buffer, 
                                VkDeviceMemory        &bufferMemory)
{
    VkBufferCreateInfo bufferInfo = {};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    
    VK_CHECK_RESULT(vkCreateBuffer(device, &bufferInfo, nullptr, &buffer),
                    "Failed to create buffer!");
    
    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(device, buffer, &memRequirements);
    
    
    VkMemoryAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = FindMemoryType(memRequirements.memoryTypeBits, 
                                               properties);
    
    VK_CHECK_RESULT(vkAllocateMemory(device, &allocInfo, nullptr, &bufferMemory),
                    "Failed to allocate buffer memory!");
    
    vkBindBufferMemory(device, buffer, bufferMemory, 0);
}


void VulkanDevice::CopyBuffer(VkBuffer srcBuffer, 
                              VkBuffer dstBuffer, 
                              VkDeviceSize size) 
{
    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = command_pool;
    allocInfo.commandBufferCount = 1;
    
    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);
    
    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    
    vkBeginCommandBuffer(commandBuffer, &beginInfo);
    
    VkBufferCopy copyRegion = {};
    copyRegion.size = size;
    vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);
    
    vkEndCommandBuffer(commandBuffer);
    
    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;
    
    vkQueueSubmit(queue_families.graphics_queue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(queue_families.graphics_queue);
    
    vkFreeCommandBuffers(device, command_pool, 1, &commandBuffer);
}

void VulkanDevice::CreateBufferAsStaging(VkBufferUsageFlags usage,
                                         VkBuffer *buffer,
                                         VkDeviceMemory *buffer_memory, 
                                         void *buffer_data, u32 size)
{
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    CreateBuffer(size, 
                 VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, 
                 stagingBufferMemory);
    
    void* data;
    vkMapMemory(device, stagingBufferMemory, 0, size, 0, &data);
    {
        memcpy(data, buffer_data, (size_t) size);
    }
    vkUnmapMemory(device, stagingBufferMemory);
    
    CreateBuffer(size,
                 VK_BUFFER_USAGE_TRANSFER_DST_BIT | usage /*VK_BUFFER_USAGE_VERTEX_BUFFER_BIT*/, 
                 VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 
                 *buffer, 
                 *buffer_memory);
    
    CopyBuffer(stagingBuffer, *buffer, size);
    
    vkDestroyBuffer(device, stagingBuffer, nullptr);
    vkFreeMemory(device, stagingBufferMemory, nullptr);
}

void VulkanDevice::Map(void **mapped_memory, VkDeviceMemory memory)
{
    vkMapMemory(device, memory, 0, VK_WHOLE_SIZE, 0, mapped_memory);
}

void VulkanDevice::Unmap(VkDeviceMemory memory)
{
    vkUnmapMemory(device, memory);
}


//-----------------------------------------------------------//
// Destroyers
//-----------------------------------------------------------//
void VulkanDevice::DestroyImageView(VkImageView view)
{
    vkDestroyImageView(device, view, nullptr);
}

void VulkanDevice::DestroyImage(VkImage image)
{
    vkDestroyImage(device, image, nullptr);
}

void VulkanDevice::FreeMemory(VkDeviceMemory memory)
{
    vkFreeMemory(device, memory, nullptr);
}

void VulkanDevice::DestroyFramebuffer(VkFramebuffer buffer)
{
    vkDestroyFramebuffer(device, buffer, nullptr);
}

void VulkanDevice::FreeCommandBuffers(VkCommandBuffer *buffers, u32 buffer_count)
{
    vkFreeCommandBuffers(device, command_pool, buffer_count, buffers);
}

void VulkanDevice::DestroySwapchainKHR(VkSwapchainKHR swapchain)
{
    vkDestroySwapchainKHR(device, swapchain, nullptr);
}

void VulkanDevice::DestroyRenderPass(VkRenderPass render_pass)
{
    vkDestroyRenderPass(device, render_pass, nullptr);
}

void VulkanDevice::Idle()
{
    vkDeviceWaitIdle(device);
}