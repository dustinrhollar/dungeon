using namespace vkMaple;
using namespace vkMaple::Base;

void Base::CreateBuffer(Device device_handle, VkDeviceSize size, VkBufferUsageFlags usage, 
                        VkMemoryPropertyFlags properties, VkBuffer& buffer, 
                        VkDeviceMemory& bufferMemory)
{
    VkBufferCreateInfo bufferInfo = {};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    
    VK_CHECK_RESULT(vkCreateBuffer(device_handle.device, &bufferInfo, nullptr, &buffer),
                    "Failed to create buffer!");
    
    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(device_handle.device, buffer, &memRequirements);
    
    VkMemoryAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = FindMemoryType(device_handle.physical_device, memRequirements.memoryTypeBits, properties);
    
    VK_CHECK_RESULT(vkAllocateMemory(device_handle.device, &allocInfo, nullptr, &bufferMemory),
                    "Failed to allocate buffer memory!");
    
    vkBindBufferMemory(device_handle.device, buffer, bufferMemory, 0);
}

//
// size is the size of each buffer
// count is the number of swap chain images
void Base::CreateUniformBuffer(Uniforms *uniforms, VkDeviceSize buffer_size, VkBufferUsageFlags usage, 
                               VkMemoryPropertyFlags properties, Device device_handle, u32 swapchain_count)
{
    uniforms->buffer_size = buffer_size;
    uniforms->usage = usage;
    uniforms->properties = properties;
    
    arrsetlen(uniforms->buffers,       swapchain_count);
    arrsetlen(uniforms->buffer_memory, swapchain_count);
    
    for (u32 i = 0; i < swapchain_count; ++i) {
        CreateBuffer(device_handle, buffer_size, usage, properties, 
                     uniforms->buffers[i], uniforms->buffer_memory[i]);
    }
}

void Base::CopyBuffer(VkDevice device, VkCommandPool command_pool, VkQueue graphics_queue,
                      VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size) {
    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = command_pool;
    allocInfo.commandBufferCount = 1;
    
    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);
    
    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    
    vkBeginCommandBuffer(commandBuffer, &beginInfo);
    
    VkBufferCopy copyRegion = {};
    copyRegion.size = size;
    vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);
    
    vkEndCommandBuffer(commandBuffer);
    
    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;
    
    vkQueueSubmit(graphics_queue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(graphics_queue);
    
    vkFreeCommandBuffers(device, command_pool, 1, &commandBuffer);
}

void Base::CreateVertexBuffer(Device device_handle, VkCommandPool command_pool, VkQueue graphics_queue,
                              VkBuffer *vertex_buffer, VkDeviceMemory *vertex_buffer_memory, void *vertices, u32 size)
{
    
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    CreateBuffer(device_handle, size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);
    
    void* data;
    vkMapMemory(device_handle.device, stagingBufferMemory, 0, size, 0, &data);
    {
        memcpy(data, vertices,  (size_t) size);
    }
    vkUnmapMemory(device_handle.device, stagingBufferMemory);
    
    CreateBuffer(device_handle, size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, *vertex_buffer, *vertex_buffer_memory);
    
    CopyBuffer(device_handle.device, command_pool, graphics_queue, stagingBuffer, *vertex_buffer, size);
    
    vkDestroyBuffer(device_handle.device, stagingBuffer, nullptr);
    vkFreeMemory(device_handle.device, stagingBufferMemory, nullptr);
}

void Base::CreateIndexBuffer(Device device_handle, VkCommandPool command_pool, VkQueue graphics_queue,
                             VkBuffer *index_buffer, VkDeviceMemory *index_buffer_memory,
                             void *indices, u32 size) {
    
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    CreateBuffer(device_handle, size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);
    
    void* data;
    vkMapMemory(device_handle.device, stagingBufferMemory, 0, size, 0, &data);
    memcpy(data, indices, (size_t) size);
    vkUnmapMemory(device_handle.device, stagingBufferMemory);
    
    CreateBuffer(device_handle, size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, *index_buffer, *index_buffer_memory);
    
    CopyBuffer(device_handle.device, command_pool, graphics_queue, stagingBuffer, *index_buffer, size);
    
    vkDestroyBuffer(device_handle.device, stagingBuffer, nullptr);
    vkFreeMemory(device_handle.device, stagingBufferMemory, nullptr);
}