using namespace vkMaple;
using namespace vkMaple::Base;

/*
Need:
VkDescriptorSetLayoutBinding *bindings
u32 layout_count
VkDescriptorType type
VkShaderStageFlags stage_flags
*/
void Base::CreateDescriptorSetLayout(VkDescriptorSetLayout *descriptorset_layout, VkDevice device,
                                     VkDescriptorSetLayoutBinding *bindings,
                                     u32 binding_count)
{
    VkDescriptorSetLayoutCreateInfo layoutInfo = {};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = binding_count;
    layoutInfo.pBindings = bindings;
    
    VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, descriptorset_layout),
                    "Failed to create descriptor set layout!");
}

// count is the number of swapchain images
void Base::CreateDescriptorPool(DescriptorPool *descriptor_pool, VkDevice device, u32 swapchain_count,
                                VkDescriptorPoolSize *pool_sizes, u32 pool_size_count)
{
    // save the size info
    arrsetlen(descriptor_pool->sizes, pool_size_count);
    for (int i = 0; i < pool_size_count; ++i)
    {
        descriptor_pool->sizes[i] = pool_sizes[i];
    }
    
    // set the descriptor count. Should this happen internally?
    for (int i = 0; i < pool_size_count; ++i)
    {
        pool_sizes[i].descriptorCount = swapchain_count;
    }
    
    VkDescriptorPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount = pool_size_count;
    poolInfo.pPoolSizes = pool_sizes;
    poolInfo.maxSets = swapchain_count;
    
    VK_CHECK_RESULT(vkCreateDescriptorPool(device, &poolInfo, nullptr, &descriptor_pool->pool),
                    "Failed to create descriptor pool!");
}

/*

*/
void Base::CreateDescriptorSets(Descriptors *descriptors, VkDevice device, u32 swapchain_count, u32 size)
{
    descriptors->layout_buffer_size = size;
    
    VkDescriptorSetLayout *layouts = nullptr;
    arrsetlen(layouts, swapchain_count);
    for (int i = 0; i < swapchain_count; ++i)
    {
        layouts[i] = descriptors->layout;
    }
    
    VkDescriptorSetAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = descriptors->descriptor_pool.pool;
    allocInfo.descriptorSetCount = swapchain_count;
    allocInfo.pSetLayouts = layouts;
    
    arrsetlen(descriptors->sets, swapchain_count);
    VK_CHECK_RESULT(vkAllocateDescriptorSets(device, &allocInfo, descriptors->sets),
                    "Failed to allocate descriptor sets!");
    
    for (int i = 0; i < swapchain_count; ++i)
    {
        VkDescriptorBufferInfo bufferInfo = {};
        bufferInfo.buffer = descriptors->mvp_uniforms.buffers[i];
        bufferInfo.offset = 0;
        bufferInfo.range = size;
        
        /*
        VkDescriptorImageInfo imageInfo = {};
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        imageInfo.imageView = textureImageView;
        imageInfo.sampler = textureSampler;
        */
        
        VkWriteDescriptorSet descriptorWrites[1] = {};
        descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[0].dstSet = descriptors->sets[i];
        descriptorWrites[0].dstBinding = 0;
        descriptorWrites[0].dstArrayElement = 0;
        descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrites[0].descriptorCount = 1;
        descriptorWrites[0].pBufferInfo = &bufferInfo;
        descriptorWrites[0].pImageInfo = nullptr; // Optional
        descriptorWrites[0].pTexelBufferView = nullptr; // Optional
        
        vkUpdateDescriptorSets(device, 1, descriptorWrites, 0, nullptr);
        
        /*
        descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[1].dstSet = descriptorSets[i];
        descriptorWrites[1].dstBinding = 1;
        descriptorWrites[1].dstArrayElement = 0;
        descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptorWrites[1].descriptorCount = 1;
        descriptorWrites[1].pImageInfo = &imageInfo;
        
        vkUpdateDescriptorSets(device, 2, descriptorWrites, 0, nullptr);
        */
        
    }
}



//