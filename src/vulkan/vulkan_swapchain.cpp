using namespace vkMaple;
using namespace vkMaple::Base;


void VulkanSwapchain::Initialize(VulkanDevice & device)
{
    Recreate(device);
}


// Can only be done once the renderpass has been created
void VulkanSwapchain::CreateFramebuffers(VulkanDevice & device, VkRenderPass & render_pass)
{
    arrsetlen(framebuffers, (u32)arrlen(image_views));
    
    for (int i = 0; i < arrlen(framebuffers); ++i)
    { 
        VkImageView attachments[] = {
            image_views[i],
            depth_resources.image_view
        };
        
        VkFramebufferCreateInfo framebufferInfo = {};
        framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferInfo.renderPass = render_pass;
        framebufferInfo.attachmentCount = 2;
        framebufferInfo.pAttachments = attachments;
        framebufferInfo.width = extent.width;
        framebufferInfo.height = extent.height;
        framebufferInfo.layers = 1;
        
        device.CreateFramebuffer(framebufferInfo, &framebuffers[i]);
    }
}

void VulkanSwapchain::CreateCommandbuffers(VulkanDevice & device)
{
    // Create the command buffers
    u32 buffer_count = arrlen(images);
    arrsetlen(command_buffers, buffer_count);
    device.CreateCommandbuffer(command_buffers, buffer_count);
}

void VulkanSwapchain::Recreate(VulkanDevice & device)
{
    // Create the swapchain
    device.CreateSwapchain(swapchain,
                           image_format,
                           extent,
                           images,
                           image_views);
    
    // Create images
    {
        u32 imageCount;
        vkGetSwapchainImagesKHR(device.device, swapchain, &imageCount, nullptr);
        
        arrsetlen(images, imageCount);
        vkGetSwapchainImagesKHR(device.device, swapchain, &imageCount, images);
    }
    
    // Create image views
    {
        arrsetlen(image_views, (u32)arrlen(images));
        
        for (u32 i = 0; i < arrlen(image_views); ++i)
        {
            image_views[i] = device.CreateImageView(images[i], 
                                                    image_format,
                                                    VK_IMAGE_ASPECT_COLOR_BIT);
        }
    }
    
    // Create depth resources
    {
        VkFormat depthFormat = device.FindDepthFormat();
        
        if (depthFormat == VK_FORMAT_UNDEFINED)
        {
            SET_ERROR_RET("Failed to find supported format!\n");
        }
        
        device.CreateImage(extent.width, extent.height, depthFormat, 
                           VK_IMAGE_TILING_OPTIMAL, 
                           VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, 
                           VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 
                           depth_resources.image, 
                           depth_resources.image_memory);
        if (depth_resources.image == VK_NULL_HANDLE) printf("Erorr creating depth image\n");
        
        depth_resources.image_view = device.CreateImageView(depth_resources.image, depthFormat, 
                                                            VK_IMAGE_ASPECT_DEPTH_BIT);
        
        device.TransitionImageLayout(depth_resources.image, depthFormat, 
                                     VK_IMAGE_LAYOUT_UNDEFINED, 
                                     VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
    }
}

void VulkanSwapchain::Cleanup(VulkanDevice & device)
{
    depth_resources.Shutdown(device);
    
    for (u32 i = 0; i < arrlen(framebuffers); ++i) {
        device.DestroyFramebuffer(framebuffers[i]);
    }
    
    device.FreeCommandBuffers(command_buffers, arrlen(command_buffers));
    
    
    for (u32 i = 0; i < arrlen(image_views); ++i) {
        device.DestroyImageView(image_views[i]);
    }
    
    device.DestroySwapchainKHR(swapchain);
}
