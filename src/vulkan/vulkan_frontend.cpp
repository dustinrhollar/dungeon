/*
TODO(Dustin): Move Vulkan application setup to this file

this file is intended to replace the api file

*/

using namespace vkMaple;

void vkMaple::BindPipeline(u32 pipeline_id)
{
    u32 imageIndex = GlobalVulkanState.frame.image_index;
    GlobalVulkanState.frame.bound_pipeline = GlobalPipelineList[pipeline_id];
    
    vkCmdBindPipeline(GlobalVulkanState.swapchain.command_buffers[imageIndex], 
                      VK_PIPELINE_BIND_POINT_GRAPHICS, 
                      GlobalVulkanState.frame.bound_pipeline.pipeline);
}

void vkMaple::BindDescriptorSet(DescritporLayoutType descriptor_type,
                                u32 first_set,
                                u32 descriptor_set_count,
                                u32 dynamic_offset_count,
                                const u32* p_dynamic_offsets)
{
    u32 image_index = GlobalVulkanState.frame.image_index;
    
    VulkanPipeline & bound_pipeline = GlobalVulkanState.frame.bound_pipeline;
    
    // TODO(Dustin): Get the other descriptor set
    DescriptorSet & object_descriptor = GlobalVulkanState.object_descriptor;
    
    VkCommandBuffer & command_buffer = 
        GlobalVulkanState.swapchain.command_buffers[image_index];
    
    vkCmdBindDescriptorSets(command_buffer, 
                            VK_PIPELINE_BIND_POINT_GRAPHICS, 
                            bound_pipeline.layout, 
                            first_set, 
                            descriptor_set_count, 
                            &object_descriptor.sets[image_index], 
                            dynamic_offset_count,
                            p_dynamic_offsets);
}
