using namespace vkMaple;
using namespace vkMaple::Platform;

// Is running/any error?
//----------------------------------------------
// bool isRunning = false;


// Error Handling
// TODO(Dustin): Precede with VK_ ?
//----------------------------------------------
#define ERROR() if (hasError) return;

#define CHECK_ERROR(act, exp, msg) \
if ((act) == (exp)) {              \
    SET_ERROR((msg));              \
}
#define CHECK_ERROR_RET(act, exp, msg) \
if ((act) == (exp)) {                  \
    SET_ERROR_RET((msg));              \
}



bool error()
{
    return hasError;
}

// Struct Defs
//----------------------------------------------


struct VkVertex
{
    Vec3 pos;
    Vec3 col;
    Vec2 tex;
    
    static VkVertexInputBindingDescription getBindingDescription() {
    }
    
    // returns a dynamic array
    static VkVertexInputAttributeDescription* getAttributeDescriptions() {
        VkVertexInputAttributeDescription *attributeDescriptions = nullptr;
        
        // Positions
        VkVertexInputAttributeDescription attribPos = {};
        attribPos.binding = 0;
        attribPos.location = 0;
        attribPos.format = VK_FORMAT_R32G32B32_SFLOAT;
        attribPos.offset = offsetof(VkVertex, pos);
        
        // Color
        VkVertexInputAttributeDescription attribCol = {};
        attribCol.binding = 0;
        attribCol.location = 1;
        attribCol.format = VK_FORMAT_R32G32B32_SFLOAT;
        attribCol.offset = offsetof(VkVertex, col);
        
        // Texture
        VkVertexInputAttributeDescription attribTex = {};
        attribTex.binding = 0;
        attribTex.location = 2;
        attribTex.format = VK_FORMAT_R32G32_SFLOAT;
        attribTex.offset = offsetof(VkVertex, tex);
        
        arrput(attributeDescriptions, attribPos);
        arrput(attributeDescriptions, attribCol);
        arrput(attributeDescriptions, attribTex);
        
        return attributeDescriptions;
    }
};

typedef struct
{
    VkImage textureImage;
    VkDeviceMemory textureImageMemory;
    VkImageView textureImageView;
} VulkanTexture;

typedef struct
{
    u32 vertexCount;
    u32 indexCount;
    u64 id; // temp
    
    VkBuffer vertexBuffer;
    VkDeviceMemory vertexBufferMemory;
    VkBuffer indexBuffer;
    VkDeviceMemory indexBufferMemory;
} VulkanMesh;

// Command Pool/Buffers: Global (for now)
global VkCommandBuffer *commandBuffers = nullptr; // dynamic array


// Synchronization: Global
global const int MAX_FRAMES_IN_FLIGHT = 2;
global size_t currentFrame = 0;
global VkSemaphore *imageAvailableSemaphore = nullptr;
global VkSemaphore *renderFinishedSemaphore = nullptr;
global VkFence *inFlightFences = nullptr;

// Uniforms and Descriptor Sets: Complicated. Per resource, BUT should go into a global pool.
global VkBuffer *uniformBuffers = nullptr; // dyn arr
global VkDeviceMemory *uniformBuffersMemory = nullptr; // dyn arr
VkDescriptorSetLayout descriptorSetLayout;
VkDescriptorPool descriptorPool;
VkDescriptorSet *descriptorSets = nullptr;

// Textures: per resource. currently hardcoded. should go into a global pool (textureList).
VkImage textureImage;
VkDeviceMemory textureImageMemory;
VkImageView textureImageView;
VkSampler textureSampler;

// Global Resources
// Key: id of the resource
// Obj: the resource information 
global std::unordered_map<u64, VulkanTexture> textureList;

internal void processCreateCommand(VkDevice device, std::vector<CreateCommand> list);
internal void processDeleteCommand(VkDevice device, std::vector<DeleteCommand> list);

internal bool createVulkanTexture(TextureInfo *info);
internal bool createVulkanMesh(Mesh *info);

// Validation layers
//----------------------------------------------
global u32 device_extensions_count = 1;
global const char *device_extensions[] = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

// Static/Internal functions
//----------------------------------------------
// General initializers
internal void initVulkan(); // init vulkan state
internal void createInstance(); // create a vulkan instance
// Validation Layers
internal bool checkValidationLayerSupport(); // check for validation layer support
//internal const char** getRequiredExtensions(); // returns a list of valid extensions if validation layers are enabled
// internal std::vector<const char*> getRequiredExtensions();
// Debug/Messengers
internal void setupDebugMessenger();
internal void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo);
// Physical/Logical/Queue Families
internal void pickPhysicalDevice(VkInstance instance, VkPhysicalDevice *physicalDevice);
internal QueueFamilyIndices findQueueFamilies(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface);
internal void createLogicalDevice(VkPhysicalDevice physicalDevice, VkDevice device, VkSurfaceKHR surface, 
                                  VkQueue *graphicsQueue, VkQueue *presentQueue);
// Surface
internal void createSurface(VkSurfaceKHR *surface);
// Swap Chain
internal bool isDeviceSuitable(VkPhysicalDevice device);
internal bool checkDeviceExtensionSupport(VkPhysicalDevice device);
internal SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR surface);
// takes in a list of available formats
internal VkSurfaceFormatKHR chooseSwapSurfaceFormat(VkSurfaceFormatKHR *availableFormats);
// takes in a list of available present modes
internal VkPresentModeKHR chooseSwapPresentMode(VkPresentModeKHR *availablePresentModes);
internal VkExtent2D chooseSwapExtent(VkSurfaceCapabilitiesKHR *capabilities);
internal void createSwapChain(VkPhysicalDevice physicalDevice, VkDevice device, VkSurfaceKHR surface);
internal void createImageViews();
// graphics pipeline
internal void createGraphicsPipeline();
internal void createRenderPass();
internal VkShaderModule createShaderModule(const char *code, int size);
// Command Buffers
internal void createFramebuffers();
internal void createCommandPool();
internal void createCommandBuffers();
internal void recordCommandBuffers(std::vector<DrawCommand> &drawnObjects, u32 imageIndex);


// Synchronization
internal void createSyncObjects(VkDevice device);
// Swap chain recreation
internal void recreateSwapChain(VkPhysicalDevice physicalDevice, VkDevice device, VkSurfaceKHR surface);
internal void cleanupSwapChain(VkDevice device);
// Model related Buffers
internal void createBuffer(VkDevice device, VkDeviceSize size, VkBufferUsageFlags usage, 
                           VkMemoryPropertyFlags properties, VkBuffer& buffer, 
                           VkDeviceMemory& bufferMemory);
internal VkCommandBuffer beginSingleTimeCommands(VkDevice device);
internal void endSingleTimeCommands(VkDevice device, VkQueue graphicsQueue, VkCommandBuffer commandBuffer);
internal void copyBuffer(VkDevice device, VkQueue graphicsQueue, VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);
internal u32 findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);
//internal void createVertexBuffers();
//internal void createIndexBuffers();
internal void createVertexBuffers(VkDevice device, VulkanMesh *vm, Vertex *vertices, u32 indCount);
internal void createIndexBuffers(VkDevice device, VulkanMesh *vm, u16 *indices, u32 indCount);


internal void createDescriptorSetLayout(VkDevice device);
internal void createUniformBuffers(VkDevice device);
internal void updateUniformBuffer(VkDevice device, u32 imageIndex);
internal void createDescriptorPool(VkDevice device);
internal void createDescriptorSets(VkDevice device);

// Image/samplers
internal void createTextureImage(VkDevice device);
internal void createImage(VkDevice device, u32 width, u32 height, VkFormat format, 
                          VkImageTiling tiling, VkImageUsageFlags usage, 
                          VkMemoryPropertyFlags properties, VkImage &image, 
                          VkDeviceMemory &imageMemory);
internal void transitionImageLayout(VkDevice device, VkImage image, VkFormat format, 
                                    VkImageLayout oldLayout, VkImageLayout newLayout);
internal void copyBufferToImage(VkDevice device, VkBuffer buffer, VkImage image, u32 width, u32 height);
//internal void createTextureImageView();
internal void createTextureSampler(VkDevice device);
internal VkImageView createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags);
// Depth Buffer creation
internal VkFormat findDepthFormat(VkPhysicalDevice physicalDevice);
internal void createDepthResources();

internal void recordCommandBuffers(std::vector<DrawCommand> &drawnObjects, u32 imageIndex)
{
    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    
    VK_CHECK_RESULT(vkBeginCommandBuffer(commandBuffers[imageIndex], &beginInfo),
                    "Failed to begin recording command buffer!");
    
    VkRenderPassBeginInfo renderPassInfo = {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass = renderPass;
    renderPassInfo.framebuffer = swapChainFramebuffers[imageIndex];
    renderPassInfo.renderArea.offset = {0, 0};
    renderPassInfo.renderArea.extent = swapChainExtent;
    
    VkClearValue clearValues[2] = {};
    clearValues[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
    clearValues[1].depthStencil = {1.0f, 0};
    
    renderPassInfo.clearValueCount = 2;
    renderPassInfo.pClearValues = clearValues;
    
    vkCmdBeginRenderPass(commandBuffers[imageIndex], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
    vkCmdBindPipeline(commandBuffers[imageIndex], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);
    
    // Record draw commands
    std::vector<DrawCommand>::iterator itr;
    for (itr = drawnObjects.begin(); itr < drawnObjects.end(); ++itr)
    {
        VulkanMesh vm = meshList[itr->meshId];
        
        VkBuffer vertexBuffers[] = {vm.vertexBuffer};
        VkDeviceSize offsets[] = {0};
        vkCmdBindVertexBuffers(commandBuffers[imageIndex], 0, 1, vertexBuffers, offsets);
        
        vkCmdBindIndexBuffer(commandBuffers[imageIndex], vm.indexBuffer, 0, VK_INDEX_TYPE_UINT32);
        
        vkCmdBindDescriptorSets(commandBuffers[imageIndex], VK_PIPELINE_BIND_POINT_GRAPHICS, 
                                pipelineLayout, 0, 1, &descriptorSets[imageIndex], 0, nullptr);
        
        vkCmdDrawIndexed(commandBuffers[imageIndex], vm.indexCount, 1, 0, 0, 0);
    }
    
    vkCmdEndRenderPass(commandBuffers[imageIndex]);
    
    VK_CHECK_RESULT(vkEndCommandBuffer(commandBuffers[imageIndex]), 
                    "Failed to record command buffer!");
}

internal void recreateSwapChain(VkPhysicalDevice physicalDevice, VkDevice device, VkSurfaceKHR surface)
{
    PlatformApi.WaitForEvents();
    
    vkDeviceWaitIdle(device);
    
    cleanupSwapChain(device);
    
    createSwapChain(physicalDevice, device, surface);
    createImageViews();
    createRenderPass();
    createGraphicsPipeline();
    createDepthResources();
    createFramebuffers();
    createUniformBuffers(device);
    createDescriptorPool(device);
    createDescriptorSets(device);
    createCommandBuffers();
}

internal void copyBuffer(VkDevice device, VkQueue graphicsQueue, VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size)
{
    VkCommandBuffer commandBuffer = beginSingleTimeCommands(device);
    
    VkBufferCopy copyRegion = {};
    copyRegion.srcOffset = 0; // Optional
    copyRegion.dstOffset = 0; // Optional
    copyRegion.size = size;
    vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);
    
    endSingleTimeCommands(device, graphicsQueue, commandBuffer);
}

internal void createVertexBuffers(VkDevice device, VkQueue graphicsQueue, VulkanMesh *vm, Vertex *vertices, u32 vertCount)
{
    VkDeviceSize bufferSize = vertCount * sizeof(Vertex);
    
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    createBuffer(device, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, 
                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 
                 stagingBuffer, stagingBufferMemory);
    
    void* data;
    vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
    {
        memcpy(data, vertices, bufferSize);
    }
    vkUnmapMemory(device, stagingBufferMemory);
    
    createBuffer(device, bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, 
                 VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 
                 vm->vertexBuffer, vm->vertexBufferMemory);
    
    copyBuffer(device, graphicsQueue, stagingBuffer, vm->vertexBuffer, bufferSize);
    
    vkDestroyBuffer(device, stagingBuffer, nullptr);
    vkFreeMemory(device, stagingBufferMemory, nullptr);
}

internal void createIndexBuffers(VkDevice device, VkQueue graphicsQueue, 
                                 VulkanMesh *vm, u32 *indices, u32 indCount)
{
    VkDeviceSize bufferSize = indCount * sizeof(indices[0]);
    
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    createBuffer(device, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, 
                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 
                 stagingBuffer, stagingBufferMemory);
    
    void* data;
    vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
    {
        memcpy(data, indices, (size_t) bufferSize);
    }
    vkUnmapMemory(device, stagingBufferMemory);
    
    createBuffer(device, bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, 
                 VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vm->indexBuffer, vm->indexBufferMemory);
    
    copyBuffer(device, graphicsQueue, stagingBuffer, vm->indexBuffer, bufferSize);
    
    vkDestroyBuffer(device, stagingBuffer, nullptr);
    vkFreeMemory(device, stagingBufferMemory, nullptr);
}

internal void updateUniformBuffer(VkDevice device, u32 currentImage)
{
    MVP ubo = {};
    ubo.model = CreateRotationMatrix({ 0.0f, 0.0f, 1.0f }, Radians(90));
    ubo.view = CreateLookAtMatrix({ 2.0f, 2.0f, 2.0f }, { 0.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, 1.0f } );
    ubo.proj = CreatePerspectiveMatrix((45.0f), swapChainExtent.width / (float) swapChainExtent.height, 0.1f, 10.0f);
    
    ubo.view[3][2] *= -1;
    ubo.proj[1][1] *= -1; 
    
    void* data;
    vkMapMemory(device, uniformBuffersMemory[currentImage], 0, sizeof(ubo), 0, &data);
    {
        memcpy(data, &ubo, sizeof(ubo));
    }
    vkUnmapMemory(device, uniformBuffersMemory[currentImage]);
}

internal bool createVulkanTexture(VkDevice device, TextureInfo *info)
{
    VulkanTexture vt;
    //VkImage textureImage;
    //VkDeviceMemory textureImageMemory;
    //VkImageView textureImageView;
    
    u32 imageSize = info->width * info->height * 4;
    if (!info->data) {
        SET_ERROR("TextureInfo does not have any data!\n");
        return false;
    }
    
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    createBuffer(device, imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, 
                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 
                 stagingBuffer, stagingBufferMemory);
    
    void* data;
    vkMapMemory(device, stagingBufferMemory, 0, imageSize, 0, &data);
    {
        memcpy(data, info->data, (size_t)(imageSize));
    }
    vkUnmapMemory(device, stagingBufferMemory);
    
    free(info->data);
    
    createImage(device, info->width, info->height, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL, 
                VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, 
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 
                vt.textureImage, vt.textureImageMemory);
    
    transitionImageLayout(device, vt.textureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED, 
                          VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    {
        copyBufferToImage(device, stagingBuffer, vt.textureImage, (u32)(info->width), (u32)(info->height));
    }
    transitionImageLayout(device, vt.textureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 
                          VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    
    vkDestroyBuffer(device, stagingBuffer, nullptr);
    vkFreeMemory(device, stagingBufferMemory, nullptr);
    
    // Create the image view
    vt.textureImageView = createImageView(vt.textureImage, VK_FORMAT_R8G8B8A8_UNORM, 
                                          VK_IMAGE_ASPECT_COLOR_BIT);
    
    textureList.insert(std::make_pair(info->textureId, vt));
    return true;
}

internal void copyBufferToImage(VkDevice device, VkBuffer buffer, VkImage image, u32 width, u32 height)
{
    VkCommandBuffer commandBuffer = beginSingleTimeCommands(device);
    
    VkBufferImageCopy region = {};
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;
    
    region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount = 1;
    
    region.imageOffset = {0, 0, 0};
    region.imageExtent = {
        width,
        height,
        1
    };
    
    vkCmdCopyBufferToImage(
        commandBuffer,
        buffer,
        image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1,
        &region
        );
    
    endSingleTimeCommands(commandBuffer);
}

internal void createTextureSampler(VkDevice device)
{
    VkSamplerCreateInfo samplerInfo = {};
    samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerInfo.magFilter = VK_FILTER_LINEAR;
    samplerInfo.minFilter = VK_FILTER_LINEAR;
    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.anisotropyEnable = VK_TRUE;
    samplerInfo.maxAnisotropy = 16;
    samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerInfo.unnormalizedCoordinates = VK_FALSE;
    samplerInfo.compareEnable = VK_FALSE;
    samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerInfo.mipLodBias = 0.0f;
    samplerInfo.minLod = 0.0f;
    samplerInfo.maxLod = 0.0f;
    
    VK_CHECK_RESULT(vkCreateSampler(device, &samplerInfo, nullptr, &textureSampler),
                    "Failed to create texture sampler!");
}

internal bool createVulkanMesh(Mesh *info) 
{
    // Create the mesh
    VulkanMesh mesh;
    mesh.id = info->key;
    mesh.vertexCount = info->vertexCount;
    mesh.indexCount  = info->indexCount;
    
    createVertexBuffers(GlobalVulkanState.device, GlobalVulkanState.graphicsQueue, 
                        &mesh, info->vertices, mesh.vertexCount);
    if (hasError) return false;
    createIndexBuffers(GlobalVulkanState.device, GlobalVulkanState.graphicsQueue, 
                       &mesh, info->indices, mesh.indexCount);
    if (hasError) return false;
    
    meshList.insert(std::make_pair(info->key, mesh));
    
    return true;
}

internal void processCreateCommand(std::vector<CreateCommand> list)
{
    // Create the new meshes
    for (auto obj : list)
    {
        createVulkanMesh(&obj.mesh);
    }
    
    // Create any new textures (all textures in list should be unique)
    for (auto obj : list)
    {
        for (u32 i = 0; i < obj.textureCount; ++i)
        {
            //createVulkanTexture(&obj.textures[i]);
        }
    }
}

internal void processDeleteCommand(VkDevice device, std::vector<DeleteCommand> list)
{ // TODO(Dustin): do the thing
    for (auto obj : list)
    {
        switch (obj.type)
        {
            case DELETE_COMMAND_MESH:
            {
                VulkanMesh mi = meshList[obj.id];
                
                vkDestroyBuffer(device, mi.vertexBuffer, nullptr);
                vkFreeMemory(device, mi.vertexBufferMemory, nullptr);
                
                vkDestroyBuffer(device, mi.indexBuffer, nullptr);
                vkFreeMemory(device, mi.indexBufferMemory, nullptr);
            } break;
            case DELETE_COMMAND_TEXTURE:
            {
                //VulkanTexture ti = textureList[obj.id];
                
                // TODO(Dustin): Will do once textures are implemented
            } break;
        }
    }
}

// Public  Functions
//----------------------------------------------
VULKAN_UPDATE(VulkanUpdate)
{
    
    glfwPollEvents();
}

VULKAN_RESIZE(VulkanResize)
{
    framebufferResized = true;
}

VULKAN_SHUTDOWN(VulkanShutdown)
{
    // Make sure the device is done rendering
    vkDeviceWaitIdle(GlobalVulkanState.device);
    
    cleanupSwapChain(GlobalVulkanState.device);
    
    vkDestroySampler(GlobalVulkanState.device, textureSampler, nullptr);
    vkDestroyImageView(GlobalVulkanState.device, textureImageView, nullptr);
    
    vkDestroyImage(GlobalVulkanState.device, textureImage, nullptr);
    vkFreeMemory(GlobalVulkanState.device, textureImageMemory, nullptr);
    
    vkDestroyDescriptorSetLayout(GlobalVulkanState.device, descriptorSetLayout, nullptr);
    
    for (auto element : meshList)
    {
        vkDestroyBuffer(GlobalVulkanState.device, element.second.vertexBuffer, nullptr);
        vkFreeMemory(GlobalVulkanState.device, element.second.vertexBufferMemory, nullptr);
        
        vkDestroyBuffer(GlobalVulkanState.device, element.second.indexBuffer, nullptr);
        vkFreeMemory(GlobalVulkanState.device, element.second.indexBufferMemory, nullptr);
    }
    
    for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
    {
        vkDestroySemaphore(GlobalVulkanState.device, renderFinishedSemaphore[i], nullptr);
        vkDestroySemaphore(GlobalVulkanState.device, imageAvailableSemaphore[i], nullptr);
        vkDestroyFence(GlobalVulkanState.device, inFlightFences[i], nullptr);
    }
    
    vkDestroyCommandPool(GlobalVulkanState.device, commandPool, nullptr);
    
    vkDestroyDevice(GlobalVulkanState.device, nullptr);
    if (enabledValidationLayers) {
        DestroyDebugUtilsMessengerEXT(GlobalVulkanState.instance, GlobalVulkanState.debugMessenger, nullptr);
    }
    
    vkDestroySurfaceKHR(GlobalVulkanState.instance, GlobalVulkanState.surface, nullptr);
    vkDestroyInstance(GlobalVulkanState.instance, nullptr);
    
    // glfwDestroyWindow(window);
    // glfwTerminate();
}