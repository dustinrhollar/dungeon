using namespace vkMaple;

void Renderer::Initialize()
{
    device.Initialize();
    
    swapchain.Initialize(device);
    
    device.CreateRenderPass(&render_pass,
                            swapchain.image_format);
    
    swapchain.CreateFramebuffers(device, render_pass);
    swapchain.CreateCommandbuffers(device);
    
    // allocator
    VmaAllocatorCreateInfo create_info = {};
    create_info.physicalDevice = device.physical_device;
    create_info.device = device.device;
    
    vmaCreateAllocator(&create_info, &allocator);
    
    background_color[0] = 0.0f;
    background_color[1] = 0.0f;
    background_color[2] = 0.0f;
}

void Renderer::SetBackgroundColor(float color[3])
{
    background_color[0] = color[0];
    background_color[1] = color[1];
    background_color[2] = color[2];
}

void Renderer::BeginFrame()
{
    vkWaitForFences(device.device, 1, 
                    &device.sync_objects.in_flight_fences[frame.current_frame], 
                    VK_TRUE, UINT64_MAX);
    
    // Draw frame
    frame.khr_result = vkAcquireNextImageKHR(device.device, 
                                             swapchain.swapchain, 
                                             UINT64_MAX, 
                                             device.sync_objects.image_available_semaphore[frame.current_frame], 
                                             VK_NULL_HANDLE, 
                                             &frame.image_index);
    
    if (frame.khr_result == VK_ERROR_OUT_OF_DATE_KHR) {
        Resize();
        return;
    } else if (frame.khr_result != VK_SUCCESS && 
               frame.khr_result != VK_SUBOPTIMAL_KHR) {
        SET_ERROR_RET("Failed to acquire swap chain image!");
    }
    
    // NOTE(Dustin): Move this to another api call?
    u32 imageIndex = frame.image_index;
    
    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    
    VK_CHECK_RESULT(vkBeginCommandBuffer(swapchain.command_buffers[imageIndex], &beginInfo),
                    "Failed to begin recording command buffer!");
    
    // Set the viewport and scissor for all pipelines
    {
        VkViewport viewport = {};
        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width  = (float) swapchain.extent.width;
        viewport.height = (float) swapchain.extent.height;
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;
        
        VkRect2D scissor = {};
        scissor.offset = {0, 0};
        scissor.extent = swapchain.extent;
        
        vkCmdSetViewport(swapchain.command_buffers[imageIndex], 0, 1, &viewport);
        vkCmdSetScissor(swapchain.command_buffers[imageIndex], 0, 1, &scissor);
    }
    
    VkRenderPassBeginInfo renderPassInfo = {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass = render_pass;
    renderPassInfo.framebuffer = swapchain.framebuffers[imageIndex];
    renderPassInfo.renderArea.offset = {0, 0};
    renderPassInfo.renderArea.extent = swapchain.extent;
    
    VkClearValue clearValues[2] = {};
    clearValues[0].color = {
        background_color[0], 
        background_color[1], 
        background_color[2], 
        1.0f
    };
    clearValues[1].depthStencil = {1.0f, 0};
    
    renderPassInfo.clearValueCount = 2;
    renderPassInfo.pClearValues = clearValues;
    
    vkCmdBeginRenderPass(swapchain.command_buffers[imageIndex], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
}

void Renderer::EndFrame()
{
    u32 imageIndex = frame.image_index;
    
    vkCmdEndRenderPass(swapchain.command_buffers[imageIndex]);
    
    VK_CHECK_RESULT(vkEndCommandBuffer(swapchain.command_buffers[imageIndex]), 
                    "Failed to record command buffer!");
    
    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    
    VkSemaphore waitSemaphores[] = {
        device.sync_objects.image_available_semaphore[frame.current_frame]
    };
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = waitSemaphores;
    submitInfo.pWaitDstStageMask = waitStages;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &swapchain.command_buffers[imageIndex];
    
    VkSemaphore signalSemaphores[] = {
        device.sync_objects.render_finished_semaphore[frame.current_frame]
    };
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = signalSemaphores;
    
    vkResetFences(device.device, 1, 
                  &device.sync_objects.in_flight_fences[frame.current_frame]);
    
    VK_CHECK_RESULT(vkQueueSubmit(device.queue_families.graphics_queue, 1, &submitInfo, 
                                  device.sync_objects.in_flight_fences[frame.current_frame]),
                    "Failed to submit draw command buffer!");
    
    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = signalSemaphores;
    
    VkSwapchainKHR swapChains[] = {swapchain.swapchain};
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;
    presentInfo.pImageIndices = &imageIndex;
    presentInfo.pResults = nullptr; // Optional
    
    vkQueuePresentKHR(device.queue_families.present_queue, &presentInfo);
    
    VkResult result = frame.khr_result;
    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || framebuffer_resized) {
        framebuffer_resized = false;
        Resize();
    } else if (result != VK_SUCCESS) {
        SET_ERROR_RET("Failed to present swap chain image!");
    }
    
    frame.current_frame = (frame.current_frame + 1) % MAX_FRAMES_IN_FLIGHT;
}

void Renderer::Resize()
{
    // Wait until device is ready for cleaning
    Platform.WaitForEvents(); 
    // Idle until last frame finishes rendering
    device.Idle();
    
    // Clean old swapchain and dependent state
    device.DestroyRenderPass(render_pass);
    
    swapchain.Cleanup(device);
    
    // Recreate new swapchain and dependent state
    swapchain.Recreate(device);
    
    device.CreateRenderPass(&render_pass,
                            swapchain.image_format);
    
    swapchain.CreateFramebuffers(device, render_pass);
    swapchain.CreateCommandbuffers(device);
    
    // TODO(Dustin): Descriptors
}

void Renderer::Shutdown()
{
    device.Idle();
    
    // TODO(Dustin): Cleanup descriptor sets 
    
    vmaDestroyAllocator(allocator);
    
    device.DestroyRenderPass(render_pass);
    
    swapchain.Cleanup(device);
    
    device.Shutdown();
}