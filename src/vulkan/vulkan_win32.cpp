// ENTRY POINT FOR VULKAN CODE

#include <optional>
#include <set>

#include <appconfig.h>

global bool hasError = false;

#define SET_ERROR(msg) \
hasError = true;   \
printf("%s\n", (char *)(msg));

#define SET_ERROR_RET(msg) \
SET_ERROR(msg);        \
return;

#define VK_CHECK_RESULT(f, msg)  \
{                            \
    VkResult res = (f);      \
    if ((res) != VK_SUCCESS) \
    {                        \
        SET_ERROR((msg));    \
    }                        \
}

// Vulkan Memory Allocator
#include <VulkanMemoryAllocator/vk_mem_alloc.h>
#include <VulkanMemoryAllocator/vk_mem_alloc.cpp>

#include <vulkan/vulkan_device.h>
#include <vulkan/vulkan_swapchain.h>
#include <vulkan/vulkan_validation.h>
#include <vulkan/vulkan_pipeline.h>
#include <vulkan/vulkan_descriptors.h>
#include <vulkan/vulkan_renderer.h>
#include <vulkan/vulkan_global.h>

/*
#include <vulkan/vulkan_command.h>
#include <vulkan/vulkan_image.h>
#include <vulkan/vulkan_buffers.h>

#include <vulkan/vulkan_command.cpp>
#include <vulkan/vulkan_buffers.cpp>
#include <vulkan/vulkan_image.cpp>
#include <vulkan/vulkan_pipeline.cpp>
#include <vulkan/vulkan_descriptors.cpp>
#include <vulkan/vulkan_api.cpp>
*/

#include <vulkan/vulkan_validation.cpp>
#include <vulkan/vulkan_device.cpp>
#include <vulkan/vulkan_swapchain.cpp>
#include <vulkan/vulkan_renderer.cpp>
