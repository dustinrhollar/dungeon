using namespace vkMaple;

bool Base::Validation::CheckValidationLayerSupport()
{
    u32 layerCount = 0;
    vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
    
    VkLayerProperties *availableLayers = nullptr;
    arrsetlen(availableLayers, layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers);
    
    for (u32 i = 0; i < validation_count; ++i)
    {
        bool layerFound = false;
        
        for (u32 j = 0; j < layerCount; ++j)
        {
            if (strcmp(validation_layers[i], availableLayers[j].layerName) == 0)
            {
                layerFound = true;
                break;
            }
        }
        
        if (!layerFound)
        {
            arrfree(availableLayers);
            return false;
        }
    }
    
    arrfree(availableLayers);
    return true;
}
