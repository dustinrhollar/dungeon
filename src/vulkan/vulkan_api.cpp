
using namespace vkMaple;
using namespace vkMaple::Base;

void vkMaple::BindPipeline(u32 pipeline_id)
{
    u32 imageIndex = GlobalVulkanState.frame.image_index;
    GlobalVulkanState.frame.bound_pipeline = GlobalPipelineList[pipeline_id];
    
    vkCmdBindPipeline(GlobalVulkanState.swapchain.command_buffers[imageIndex], 
                      VK_PIPELINE_BIND_POINT_GRAPHICS, 
                      GlobalVulkanState.frame.bound_pipeline.pipeline);
}


void vkMaple::DrawMesh(DrawMeshInfo mesh_info)
{
    u32 imageIndex = GlobalVulkanState.frame.image_index;
    
    MeshBuffer mesh_buffer = GlobalMeshList[mesh_info.mesh_id];
    
    VkBuffer vertexBuffers[] = {mesh_buffer.vertex_buffer};
    VkDeviceSize offsets[] = {0};
    vkCmdBindVertexBuffers(GlobalVulkanState.swapchain.command_buffers[imageIndex], 
                           0, 1, vertexBuffers, offsets);
    
    vkCmdBindIndexBuffer(GlobalVulkanState.swapchain.command_buffers[imageIndex], 
                         mesh_buffer.index_buffer, 0, VK_INDEX_TYPE_UINT16);
    
    vkCmdBindDescriptorSets(GlobalVulkanState.swapchain.command_buffers[GlobalVulkanState.frame.image_index], 
                            VK_PIPELINE_BIND_POINT_GRAPHICS, 
                            GlobalVulkanState.frame.bound_pipeline.layout, 
                            0, 1, 
                            &GlobalVulkanState.descriptors.sets[GlobalVulkanState.frame.image_index], 
                            0, nullptr);
    
    vkCmdDrawIndexed(GlobalVulkanState.swapchain.command_buffers[imageIndex], 
                     static_cast<uint32_t>(mesh_buffer.mesh_info.index_count), 1, 0, 0, 0);
}

void vkMaple::Resize()
{
    framebufferResized = true;
}

void VulkanShutdown()
{
}

void vkMaple::CreateMesh(CreateMeshInfo mesh_info)
{
    MeshBuffer buffer;
    buffer.mesh_info = mesh_info;
    
    CreateVertexBuffer(GlobalVulkanState.device_handle, GlobalVulkanState.command_pool, 
                       GlobalVulkanState.queue_families.graphics_queue,
                       &buffer.vertex_buffer, &buffer.vertex_buffer_memory,
                       mesh_info.vertices, mesh_info.vert_size);
    CreateIndexBuffer(GlobalVulkanState.device_handle, GlobalVulkanState.command_pool, 
                      GlobalVulkanState.queue_families.graphics_queue,
                      &buffer.index_buffer, &buffer.index_buffer_memory,
                      mesh_info.indices, mesh_info.index_size);
    
    
    GlobalMeshList.insert(std::make_pair(mesh_info.mesh_id, buffer));
}

void vkMaple::CreateUniformBuffer(VkDeviceSize buffer_size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties)
{
    Base::CreateUniformBuffer(&GlobalVulkanState.descriptors.mvp_uniforms, buffer_size, usage, 
                              properties, GlobalVulkanState.device_handle, arrlen(GlobalVulkanState.swapchain.images));
}

void vkMaple::CreateDescriptorPool(VkDescriptorPoolSize *pool_sizes, u32 pool_size_count)
{
    Base::CreateDescriptorPool(&GlobalVulkanState.descriptors.descriptor_pool, GlobalVulkanState.device_handle.device, 
                               arrlen(GlobalVulkanState.swapchain.images),
                               pool_sizes, pool_size_count);
}

// TODO(Dustin): Actually make this call configurable please
void vkMaple::CreateDescriptorSets(u32 size)
{
    Base::CreateDescriptorSets(&GlobalVulkanState.descriptors, GlobalVulkanState.device_handle.device, 
                               arrlen(GlobalVulkanState.swapchain.images), size);
}

void vkMaple::BindDescriptors()
{
}

void vkMaple::Map(void **mapped_memory)
{
    vkMapMemory(GlobalVulkanState.device_handle.device, 
                GlobalVulkanState.descriptors.mvp_uniforms.buffer_memory[GlobalVulkanState.frame.image_index], 
                0, VK_WHOLE_SIZE, 0, mapped_memory);
}

void vkMaple::Unmap()
{
    vkUnmapMemory(GlobalVulkanState.device_handle.device, 
                  GlobalVulkanState.descriptors.mvp_uniforms.buffer_memory[GlobalVulkanState.frame.image_index]);
}