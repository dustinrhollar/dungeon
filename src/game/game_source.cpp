// C/C++ lib
#include <iostream>
#include <vector>
#include <unordered_map>
#include <map>
#include <stdlib.h>
#include <stdio.h>

#include <vulkan/vulkan.h>

// GMath
#ifndef GMATH_IMPLEMENTATION
#define GMATH_IMPLEMENTATION
#endif
#ifndef GMATH_DEPTH_ZERO_TO_ONE
#define GMATH_DEPTH_ZERO_TO_ONE
#endif
#include <GMath/GMath.h>

#define STB_IMAGE_IMPLEMENTATION
#define STB_DS_IMPLEMENTATION
#include <stb/stb_image.h>
#include <stb/stb_ds.h>

// Utils
#define MAPLE_ARRAY_LIST_IMPLEMENTATION
#include <utils/array_list.h>

// Headers
#include <appconfig.h>
#include <dungeon_platform.h>

// GLOBALS TEMPORARY
platform_api Platform;

typedef struct
{
    Vec3 pos;
    Vec3 col;
    Vec2 tex;
} Vertex;

// Vulkan source
#include <vulkan/vulkan_win32.cpp>

#include <dungeon.h>

// Source
#include "dungeon.cpp"