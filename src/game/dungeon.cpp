/*
// TODO(Dustin): 
Fix hotloading of code!

*/

struct MaterialParameters
{
    Vec4 color; // color of a tile
};

struct Material
{
    VkDescriptorSetLayout layout;
    VkDescriptorSet *sets = nullptr; // dynamic array
    u32 pipeline_id;
    
    MaterialParameters params;
};

typedef struct
{
    VkBuffer       vertex_buffer;
    VmaAllocation  vertex_allocation;
    VkBuffer       index_buffer;
    VmaAllocation  index_allocation;
    
    // Binds the vertex buffers
    void BindVertexBuffer(VkCommandBuffer & command_buffer,
                          u32 first_binding = 0,
                          u32 binding_count = 1)
    {
        VkBuffer vertexBuffers[] = {vertex_buffer};
        VkDeviceSize offsets[] = {0};
        vkCmdBindVertexBuffers(command_buffer, 
                               0, 
                               1, 
                               vertexBuffers, 
                               offsets);
    }
    
    void BindIndexBuffer(VkCommandBuffer & command_buffer,
                         u32 vertex_offset)
    {
        
        vkCmdBindIndexBuffer(command_buffer, 
                             index_buffer, 
                             vertex_offset, 
                             VK_INDEX_TYPE_UINT16);
        
    }
    
    void Shutdown(VmaAllocator &allocator)
    {
        vmaDestroyBuffer(allocator, vertex_buffer, vertex_allocation);
        vmaDestroyBuffer(allocator, index_buffer, index_allocation);
    }
} MeshBuffer;

struct Mesh
{
    u32 index_offset; // not in use right now, offset into the index buffer
    u32 index_count;
    u32 ubo_offset;
    
    MeshBuffer device_memory;
    
    void DrawIndexed(VkCommandBuffer & command_buffer,
                     u32 instance_count,
                     u32 first_index,
                     u32 vertex_offset,
                     u32 first_instance)
    {
        device_memory.BindVertexBuffer(command_buffer);
        
        device_memory.BindIndexBuffer(command_buffer, vertex_offset);
        
        vkCmdDrawIndexed(command_buffer, 
                         index_count,
                         instance_count,
                         first_index, 
                         vertex_offset, 
                         first_instance);
    }
    
    //Material material;
};

#define MAX_RESOURCE_PATH 256
typedef char Resources[MAX_RESOURCE_PATH];
struct Scene
{
    Resources shader_resources;
    Resources model_resources;
    
    DescriptorPool descriptor_pool;
    DescriptorSet  descriptor_set;
    Uniform        uniform;
    VulkanPipeline pipeline;
    
    std::unordered_map<u32, Mesh*>          mesh_list;
    std::unordered_map<u32, VulkanPipeline> pipeline_list;
    
    //VkDeviceMemory mvp_memory;
    //VkBuffer mvp_buffer;
} scene;

// TODO(Dustin): Move somewhere else
struct VulkanViewProjection
{
    alignas(16) Mat4 view;
    alignas(16) Mat4 proj;
};

typedef struct
{
    alignas(16) Mat4 model;
    alignas(16) Mat4 view;
    alignas(16) Mat4 proj;
} VulkanModel;

global u32 pipeline_id = -1;



Renderer renderer;

void CreateMesh(Renderer & renderer, 
                Mesh & mesh, 
                CreateMeshInfo mesh_info)
{
    VulkanDevice & device = renderer.device;
    MeshBuffer & buffer   = mesh.device_memory;
    
    mesh.index_count = mesh_info.index_count;
    
    // Vertex Buffer
    {
        // Create staging buffer and map the memory to it
        VkBuffer       staging_buffer;
        VmaAllocation  staging_allocation;
        
        VkBufferCreateInfo staging_buffer_info = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
        staging_buffer_info.size = mesh_info.vert_size;
        staging_buffer_info.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        
        VmaAllocationCreateInfo alloc_info = {};
        alloc_info.usage = VMA_MEMORY_USAGE_CPU_ONLY;
        
        vmaCreateBuffer(renderer.allocator, 
                        &staging_buffer_info, 
                        &alloc_info, 
                        &staging_buffer, 
                        &staging_allocation, 
                        nullptr);
        
        void *mapped_data;
        vmaMapMemory(renderer.allocator, staging_allocation, &mapped_data);
        {
            memcpy(mapped_data, mesh_info.vertices, mesh_info.vert_size);
        }
        vmaUnmapMemory(renderer.allocator, staging_allocation);
        
        // Create the vertex buffer
        VkBufferCreateInfo vertex_buffer_info = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
        vertex_buffer_info.size = mesh_info.vert_size;
        vertex_buffer_info.usage = 
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
        
        VmaAllocationCreateInfo vertex_alloc_info = {};
        vertex_alloc_info.usage = VMA_MEMORY_USAGE_GPU_ONLY;
        
        vmaCreateBuffer(renderer.allocator, 
                        &vertex_buffer_info, 
                        &vertex_alloc_info, 
                        &buffer.vertex_buffer, 
                        &buffer.vertex_allocation, 
                        nullptr);
        
        // Copy from staging to vertex buffer
        renderer.device.CopyBuffer(staging_buffer, buffer.vertex_buffer, mesh_info.vert_size);
        
        // Free up staging
        vmaDestroyBuffer(renderer.allocator, staging_buffer, staging_allocation);
    }
    
    // Index Buffer
    {
        // Create staging buffer and map the memory to it
        VkBuffer       staging_buffer;
        VmaAllocation  staging_allocation;
        
        VkBufferCreateInfo staging_buffer_info = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
        staging_buffer_info.size = mesh_info.index_size;
        staging_buffer_info.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        
        VmaAllocationCreateInfo alloc_info = {};
        alloc_info.usage = VMA_MEMORY_USAGE_CPU_ONLY;
        
        vmaCreateBuffer(renderer.allocator, 
                        &staging_buffer_info, 
                        &alloc_info, 
                        &staging_buffer, 
                        &staging_allocation, 
                        nullptr);
        
        void *mapped_data;
        vmaMapMemory(renderer.allocator, staging_allocation, &mapped_data);
        {
            memcpy(mapped_data, mesh_info.indices, mesh_info.index_size);
        }
        vmaUnmapMemory(renderer.allocator, staging_allocation);
        
        // Create the index buffer
        VkBufferCreateInfo index_buffer_info = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
        index_buffer_info.size = mesh_info.index_size;
        index_buffer_info.usage = 
            VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
        
        VmaAllocationCreateInfo index_alloc_info = {};
        index_alloc_info.usage = VMA_MEMORY_USAGE_GPU_ONLY;
        
        vmaCreateBuffer(renderer.allocator, 
                        &index_buffer_info, 
                        &index_alloc_info, 
                        &buffer.index_buffer, 
                        &buffer.index_allocation, 
                        nullptr);
        
        // Copy from staging to vertex buffer
        renderer.device.CopyBuffer(staging_buffer, buffer.index_buffer, mesh_info.index_size);
        
        // Free up staging
        vmaDestroyBuffer(renderer.allocator, staging_buffer, staging_allocation);
    }
}

void CreateOpaquePipeline(VulkanPipeline & pipeline,
                          VkDescriptorSetLayout descriptor_set_layout)
{
    // Descriptor set layout
    VkDescriptorSetLayoutBinding bindings[1]= {};
    bindings[0].binding            = 0;
    bindings[0].descriptorType     = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    bindings[0].descriptorCount    = 1;
    bindings[0].stageFlags         = VK_SHADER_STAGE_VERTEX_BIT;
    bindings[0].pImmutableSamplers = nullptr; // Optional
    
    //------------------------------------
    // Read in the shader file
    FileInfo vertInfo;
    FileInfo fragInfo;
    const char *vertF = "resources/shaders/vert.spv";
    const char *fragF = "resources/shaders/frag.spv";
    
    // TODO(Dustin): Move to using platoform api
    Platform.ReadFile(&vertInfo, vertF);
    Platform.ReadFile(&fragInfo, fragF);
    
    if (vertInfo.contents == NULL || fragInfo.contents == NULL)
    {
        printf("Unable to load shaders!\n");
    }
    //
    
    PipelineCreateInfo info;
    info.vert_size  = vertInfo.size;
    info.frag_size  = fragInfo.size;
    info.vert_code  = vertInfo.contents;
    info.frag_code  = fragInfo.contents;
    
    VkVertexInputBindingDescription vertex_desc[1] = {
        vkMaple::CreateVertexBindingDescription(0, sizeof(Vertex), VK_VERTEX_INPUT_RATE_VERTEX)
    };
    
    VkVertexInputAttributeDescription attribute_descriptions[] = {
        /* Position */
        vkMaple::CreateVertexAttributeDescription(0,                          // binding 
                                                  0,                          // location
                                                  VK_FORMAT_R32G32B32_SFLOAT, // format 
                                                  offsetof(Vertex, pos)),     // offset
        /* Color */
        vkMaple::CreateVertexAttributeDescription(0, 
                                                  1, 
                                                  VK_FORMAT_R32G32B32_SFLOAT, 
                                                  offsetof(Vertex, col)),
        /* Texture */
        vkMaple::CreateVertexAttributeDescription(0, 
                                                  2, 
                                                  VK_FORMAT_R32G32_SFLOAT, 
                                                  offsetof(Vertex, tex)),
    };
    
    info.vertex_desc_count = sizeof(vertex_desc) / sizeof(vertex_desc[0]);
    info.vertex_desc = vertex_desc;
    info.attrib_count = sizeof(attribute_descriptions) / sizeof(attribute_descriptions[0]);
    info.attribute_descriptions = attribute_descriptions;
    
    pipeline.Initialize(info, 
                        descriptor_set_layout,
                        renderer.render_pass,
                        renderer.swapchain.extent,
                        renderer.device);
    
    
    // Free the file info manually for now
    {
        free(vertInfo.contents);
        free(fragInfo.contents);
        
        vertInfo.size = 0;
        fragInfo.size = 0;
    }
}

void InitializeGraphics()
{
    u64 start = Platform.GetWallClock();
    
    renderer.Initialize();
    
    float bg[3] = {
        0.5f, 0.1f, 0.3f
    };
    renderer.SetBackgroundColor(bg);
    
    u64 end = Platform.GetWallClock();
    printf("Time to initialize vulkan %9.6fs\n", Platform.GetSecondsElapsed(start, end));
}

void InitializeWorld(Scene & scene)
{
    u32 swapchain_count = arrlen(renderer.swapchain.images);
    
    // Descriptor Set Layout
    VkDescriptorSetLayout layout;
    {
        VkDescriptorSetLayoutBinding bindings[1]= {};
        bindings[0].binding            = 0;
        bindings[0].descriptorType     = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
        bindings[0].descriptorCount    = 1;
        bindings[0].stageFlags         = VK_SHADER_STAGE_VERTEX_BIT;
        bindings[0].pImmutableSamplers = nullptr; // Optional
        
        // BRING FUNCTION HERE
        renderer.device.CreateDescriptorSetLayout(&layout, 
                                                  bindings,
                                                  sizeof(bindings) / sizeof(bindings[0]));
    }
    
    // Create Descriptor Pool
    {
        // set the contents of what you can allocate from the pool
        u32 pool_count = 1;
        arrsetlen(scene.descriptor_pool.sizes, pool_count);
        scene.descriptor_pool.sizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
        scene.descriptor_pool.sizes[0].descriptorCount = swapchain_count;
        
        renderer.device.CreateDescriptorPool(&scene.descriptor_pool.pool,
                                             scene.descriptor_pool.sizes,
                                             pool_count,
                                             swapchain_count);
    }
    
    // Create MVP uniforms
    {
        VkBufferUsageFlags usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
        VkMemoryPropertyFlags properties = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | 
            VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
        
        size_t buffer_size = sizeof(VulkanModel) * 2;
        
        size_t minUboAlignment = renderer.GetMinMemoryAlignment();
        if (minUboAlignment > 0) {
            buffer_size = 
                (buffer_size + minUboAlignment - 1) & ~(buffer_size - 1);
        }
        
        Uniform & object_uniforms = scene.uniform;
        
        // Memory alignment for ONE VulkanModel, used for offsets into the buffer
        size_t min_mem_alignment = renderer.GetMinMemoryAlignment();
        size_t min_ubo_alignment = sizeof(VulkanModel);
        object_uniforms.min_ubo_alignment = (min_ubo_alignment + min_mem_alignment - 1) & 
            ~(min_ubo_alignment - 1);
        
        // Create buffers
        arrsetlen(object_uniforms.buffers,    swapchain_count);
        arrsetlen(object_uniforms.allocation, swapchain_count);
        arrsetlen(object_uniforms.alloc_info, swapchain_count);
        
        object_uniforms.buffer_size = buffer_size;
        object_uniforms.usage       = usage;
        object_uniforms.properties  = properties;
        
        for (u32 i = 0; i < swapchain_count; ++i)
        {
            VkBufferCreateInfo create_info = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
            create_info.size = buffer_size;
            create_info.usage = usage;
            
            // Allow for the buffers to persistently mapped.
            // Can be accessed using alloc_info.pMappedData
            VmaAllocationCreateInfo alloc_info = {};
            alloc_info.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
            alloc_info.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;
            
            vmaCreateBuffer(renderer.allocator, 
                            &create_info, 
                            &alloc_info, 
                            &object_uniforms.buffers[i], 
                            &object_uniforms.allocation[i],
                            &object_uniforms.alloc_info[i]);
        }
    }
    
    // Create Descriptor Set
    {
        DescriptorPool & descriptor_pool = scene.descriptor_pool;
        DescriptorSet & object_descriptor = scene.descriptor_set;
        Uniform & object_uniforms = scene.uniform;
        
        VkDescriptorSetLayout *layouts = nullptr;
        arrsetlen(layouts, swapchain_count);
        for (int i = 0; i < swapchain_count; ++i)
        {
            layouts[i] = layout;
        }
        
        VkDescriptorSetAllocateInfo allocInfo = {};
        allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        allocInfo.descriptorPool     = descriptor_pool.pool;
        allocInfo.descriptorSetCount = swapchain_count;
        allocInfo.pSetLayouts        = layouts;
        
        arrsetlen(object_descriptor.sets, swapchain_count);
        renderer.device.CreateDescriptorSets(object_descriptor.sets,
                                             allocInfo);
        
        for (u32 i = 0; i < swapchain_count; ++i)
        {
            VkWriteDescriptorSet descriptorWrites[1] = {};
            
            // Set 0: MVP matrices
            // NOTE(Dustin): Watch out for subalocation
            VkDescriptorBufferInfo bufferInfo = {};
            bufferInfo.buffer                 = object_uniforms.buffers[i];
            bufferInfo.offset                 = 0;
            bufferInfo.range                  = VK_WHOLE_SIZE; //object_uniforms.buffer_size;
            
            // NOTE(Dustin): Binding is currently hardcoded.
            descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            descriptorWrites[0].dstSet = object_descriptor.sets[i];
            descriptorWrites[0].dstBinding      = 0;
            descriptorWrites[0].dstArrayElement = 0;
            descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
            descriptorWrites[0].descriptorCount = 1;
            descriptorWrites[0].pBufferInfo = &bufferInfo;
            descriptorWrites[0].pImageInfo = nullptr; // Optional
            descriptorWrites[0].pTexelBufferView = nullptr; // Optional
            
            renderer.device.UpdateDescriptorSets(descriptorWrites,
                                                 sizeof(descriptorWrites) / sizeof(descriptorWrites[0]));
        }
        
    }
    
    // Create Pipeline
    pipeline_id = scene.pipeline_list.size();
    {
        VulkanPipeline pipeline;
        CreateOpaquePipeline(pipeline, layout);
        scene.pipeline_list.insert(std::make_pair(pipeline_id, pipeline));
    }
    
    
    u64 start = Platform.GetWallClock();
    Vertex verts2[] = {
        {{-0.5f, -0.5f, 0.0}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
        {{0.5f, -0.5f, 0.0},  {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f}},
        {{0.5f, 0.5f, 0.0},   {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f}},
        {{-0.5f, 0.5f, 0.0},  {1.0f, 1.0f, 1.0f}, {0.0f, 0.0f}}
    };
    
    Vertex vertices[] = {
        {{-0.5f, -0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
        {{0.5f, -0.5f, -0.5f},  {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f}},
        {{0.5f, 0.5f, -0.5f},   {1.0f, 0.0f, 1.0f}, {1.0f, 1.0f}},
        {{-0.5f, 0.5f, -0.5f},  {1.0f, 1.0f, 1.0f}, {0.0f, 1.0f}},
    };
    
    u16 indices[] = {
        0, 1, 2, 2, 3, 0
    };
    
    u32 current_ubo_offset = 0;
    u32 ubo_stride = scene.uniform.min_ubo_alignment; 
    
    // MESH 1
    {
        Mesh *mesh = (Mesh*)malloc(sizeof(Mesh));
        CreateMeshInfo mesh_info;
        mesh_info.vertices = vertices;
        mesh_info.vert_count = sizeof(vertices) / sizeof(vertices[0]);
        mesh_info.vert_size = sizeof(vertices);
        
        mesh_info.indices = indices;
        mesh_info.index_count = sizeof(indices) / sizeof(indices[0]);
        mesh_info.index_size = sizeof(indices);
        
        mesh_info.mesh_id = scene.mesh_list.size();
        
        CreateMesh(renderer, *mesh, mesh_info);
        mesh->ubo_offset = current_ubo_offset;
        current_ubo_offset += ubo_stride;
        
        u32 mesh_id = scene.mesh_list.size();
        scene.mesh_list.insert(std::make_pair(mesh_id, mesh));
    }
    
    // MESH 2
    {
        Mesh *mesh = (Mesh*)malloc(sizeof(Mesh));
        CreateMeshInfo mesh_info;
        mesh_info.vertices = verts2;
        mesh_info.vert_count = sizeof(verts2) / sizeof(verts2[0]);
        mesh_info.vert_size = sizeof(verts2);
        
        mesh_info.indices = indices;
        mesh_info.index_count = sizeof(indices) / sizeof(indices[0]);
        mesh_info.index_size = sizeof(indices);
        
        mesh_info.mesh_id = scene.mesh_list.size();
        
        CreateMesh(renderer, *mesh, mesh_info);
        mesh->ubo_offset = current_ubo_offset;
        current_ubo_offset += ubo_stride;
        
        u32 mesh_id = scene.mesh_list.size();
        scene.mesh_list.insert(std::make_pair(mesh_id, mesh));
    }
    
    u64 end = Platform.GetWallClock();
    printf("Time to create world  %9.6fs\n", Platform.GetSecondsElapsed(start, end));
}

GAME_INITIALIZE(GameInitialize)
{
    Platform = platform;
    
    char buffer[256];
    
    char *shader_resources_filepath = "resources\\shaders\\";
    char *model_resources_filepath  = "resources\\models\\";
    
    Platform.GetFullFilepath(scene.shader_resources, MAX_RESOURCE_PATH, 
                             shader_resources_filepath, sizeof(shader_resources_filepath));
    Platform.GetFullFilepath(scene.model_resources, MAX_RESOURCE_PATH, 
                             model_resources_filepath, sizeof(model_resources_filepath));
    
    
    printf("%s\n", scene.shader_resources);
    printf("%s\n", scene.model_resources);
    
    InitializeGraphics();
    
    InitializeWorld(scene);
}


void AnimateDoubleMesh(Uniform & uniform, u32 image_index)
{
    local_persist float rotator = 0.0f;
    rotator += 0.00005;
    
    local_persist float rotator2 = 0.0f;
    rotator2 -= 0.00010;
    
    // calculate the matrices
    VulkanModel ubo[2] = {};
    ubo[0].model = CreateRotationMatrix({ 0.0f, 0.0f, 1.0f }, rotator * (90.0f));
    ubo[0].view = CreateLookAtMatrix({ 2.0f, 2.0f, 2.0f }, { 0.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, 1.0f } );
    
    int width, height;
    Platform.GetFramebufferSize(&width, &height);
    ubo[0].proj = CreatePerspectiveMatrix((45.0f), width / (float) height, 0.1f, 10.0f);
    
    ubo[0].view[3][2] *= -1;
    ubo[0].proj[1][1] *= -1;
    
    // Mesh 2
    ubo[1].model = CreateRotationMatrix({ 0.0f, 0.0f, 1.0f }, rotator2 * (90.0f));
    ubo[1].view = CreateLookAtMatrix({ 2.0f, 2.0f, 2.0f }, { 0.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, 1.0f } );
    
    ubo[1].proj = CreatePerspectiveMatrix((45.0f), width / (float) height, 0.1f, 10.0f);
    ubo[1].view[3][2] *= -1;
    ubo[1].proj[1][1] *= -1;
    
    
    // update the memory
    void *data = uniform.alloc_info[image_index].pMappedData;
    {
        u32 min_ubo_alignment = uniform.min_ubo_alignment;
        
        size_t off1 = min_ubo_alignment * 0;
        size_t off2 = min_ubo_alignment * 1;
        
        void *uboA = (void*)((char*)data + off1);
        void *uboB = (void*)((char*)data + off2);
        
        VulkanModel *modelA = (VulkanModel*)uboA;
        VulkanModel *modelB = (VulkanModel*)uboB;
        
        modelA->view  = ubo[0].view;
        modelA->proj  = ubo[0].proj;
        modelA->model = CreateRotationMatrix({ 0.0f, 0.0f, 1.0f }, rotator * (90.0f));
        
        modelB->view  = ubo[1].view;
        modelB->proj  = ubo[1].proj;
        modelB->model = CreateRotationMatrix({ 0.0f, 0.0f, 1.0f }, rotator2 * (90.0f));
    }
}


GAME_UPDATE(GameUpdate)
{
    renderer.BeginFrame();
    
    // Frame info
    u32 image_index = renderer.frame.image_index;
    VkCommandBuffer & buffer = renderer.swapchain.command_buffers[image_index];
    
    // Animate
    AnimateDoubleMesh(scene.uniform, renderer.frame.image_index);
    u32 min_ubo_alignment = scene.uniform.min_ubo_alignment;
    
    // Get the only pipeline
    VulkanPipeline & pipeline = scene.pipeline_list[pipeline_id];
    pipeline.Bind(buffer);
    
    u32 i = 0;
    for (auto mesh : scene.mesh_list)
    {
        Mesh * render_mesh = mesh.second;
        
        scene.descriptor_set.Bind(buffer,
                                  pipeline.layout,
                                  image_index,
                                  0, // first set
                                  1, // descriptor set count
                                  1, // offset count
                                  &render_mesh->ubo_offset);
        
        render_mesh->DrawIndexed(buffer, 1, 0, 0, 0);
        ++i;
    }
    
    renderer.EndFrame();
}

GAME_RESIZE(GameResize)
{
    vkMaple::framebuffer_resized = true;
}


void GameShutdown()
{
    //vkMaple::ShutdownGraphics();
}

/*
bool error()
{
    //return hasError;
}
*/
