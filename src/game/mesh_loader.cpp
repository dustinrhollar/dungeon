#define CGLTF_IMPLEMENTATION
#include <cgltf/cgltf.h>

typedef enum
{
    MESH_TYPE_GLTF,
    MESH_TYPE_OBJ,
} MeshType;

typedef struct
{

} MeshLoader;

void loadGLTFMesh(Mesh *mesh, const char *filename)
{
    cgltf_options options = {0};
    cgltf_data *data = NULL;
    cgltf_result result = cgltf_parse_file(&options, "scene.gltf", &data);
    if (result == cgltf_result_success)
    {
        printf("Unable to load glTF file!\n");
        cgltf_free(data);
    }
}

void loadMesh(Mesh *mesh, const char *filename)
{
    // TODO(Dustin): Check the file extension, but for now, assume the file is glTF

    loadMeshGLTF(mesh, filename)
}